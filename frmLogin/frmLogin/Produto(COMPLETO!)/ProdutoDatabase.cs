﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"insert into tb_produto
                            (
                            nm_marca,
                            vl_preco,                           
                            id_fornecedor
                            )
                            values 
                            (
                            @nm_marca,
                            @vl_preco,                           
                            @id_fornecedor
                            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_marca", dto.Marca));
            parms.Add(new MySqlParameter("vl_preco", dto.Preço));
            parms.Add(new MySqlParameter("id_fornecedor", dto.Fornecedor));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public List<ProdutoDTO> Listar()
        {
            string script = "select * from tb_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();

            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.ID = reader.GetInt32("id_produto");
                dto.Marca = reader.GetString("nm_marca");
                dto.Preço = reader.GetDecimal("vl_preco");
                dto.Fornecedor = reader.GetInt32("id_fornecedor");


                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public void Alterar(ProdutoDTO dto)
        {
            string script = @"UPDATE tb_produto
                                SET id_produto = @id_produto,
                                    nm_marca = @nm_marca , 
                                    vl_preco = @vl_preco ,
                                    id_fornecedor = @id_fornecedor    
                                WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.ID));
            parms.Add(new MySqlParameter("nm_marca", dto.Marca));
            parms.Add(new MySqlParameter("vl_preco", dto.Preço));
            parms.Add(new MySqlParameter("id_fornecedor", dto.Fornecedor));




            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public List<ProdutoDTO> Consultar(ProdutoDTO dto)
        {
            string script = "select * from tb_produto WHERE nm_marca = @nm_marca";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_marca", "%" + dto.Marca + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();

            while (reader.Read())

            {
                ProdutoDTO dtos= new ProdutoDTO();
                dtos.ID = reader.GetInt32("id_produto");
                dtos.Marca = reader.GetString("nm_marca");
                dtos.Preço = reader.GetDecimal("vl_preco");
                dtos.Fornecedor = reader.GetInt32("id_fornecedor");
                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }
    }
}
