﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Produto
{
    class ProdutoDTO
    {
        public int ID { get; set; }

        public string Marca { get; set; }

        public decimal Preço { get; set; }


        public int Fornecedor { get; set; }

    }
}
