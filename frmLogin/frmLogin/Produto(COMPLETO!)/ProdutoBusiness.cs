﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Produto
{
    class ProdutoBusiness
    {
        ProdutoDatabase db = new ProdutoDatabase();

        public int Salvar(ProdutoDTO dto)
        {
            return db.Salvar(dto);
        }
        public void Remover(int ID)
        {
            db.Remover(ID);

        }

        public List<ProdutoDTO> Listar()
        {
            return db.Listar();
        }
        public void Alterar(ProdutoDTO dto)
        {
            db.Alterar(dto);
        }
        public List<ProdutoDTO> Consultar(ProdutoDTO dto)
        {
            return db.Consultar(dto);
        }
    }
}
