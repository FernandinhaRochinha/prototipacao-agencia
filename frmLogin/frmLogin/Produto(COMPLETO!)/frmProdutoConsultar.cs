﻿using frmLogin.Acesso;
using frmLogin.Produto;
using frmLogin.Telas.Cadastros;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Telas.Consultas
{
    public partial class frmProdutoConsultar : Form
    {
        public frmProdutoConsultar()
        {
            InitializeComponent();
        }

        void Consultar()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> listar = business.Listar();

            dgvProduto.AutoGenerateColumns = false;
            dgvProduto.DataSource = listar;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmProdutoConsultar_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Consultar();



        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Hide();
        }

        private void dgvProduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                ProdutoDTO dto = dgvProduto.CurrentRow.DataBoundItem as ProdutoDTO;
                DialogResult resp = MessageBox.Show("Deseja realmente excluir?", "Viagem", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (resp == DialogResult.Yes)
                {
                    AcessoBusiness acessoBusiness = new AcessoBusiness();
                    acessoBusiness.Remover(dto.ID);

                    ProdutoBusiness business = new ProdutoBusiness();
                    business.Remover(dto.ID);
                    MessageBox.Show("Produto Removido com sucesso!", "Viagem", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Consultar();
                }
            }
            if (e.ColumnIndex == 6)
            {
                ProdutoDTO dto = dgvProduto.CurrentRow.DataBoundItem as ProdutoDTO;


            }
        }
    }
}
