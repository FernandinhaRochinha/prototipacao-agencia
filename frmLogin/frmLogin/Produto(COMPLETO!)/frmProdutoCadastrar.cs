﻿using frmLogin.Fornecedor;
using frmLogin.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Telas.Cadastros
{
    public partial class frmProdutoCadastrar : Form
    {
        public frmProdutoCadastrar()
        {
            InitializeComponent();
            CarregarCombo();
        }
        void CarregarCombo()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.Id);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Fornecedor);

            cboFornecedor.DataSource = lista;




        }

        private void frmProdutoCadastrar_Load(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FornecedorDTO fofo = cboFornecedor.SelectedItem as FornecedorDTO;
            ProdutoDTO dto = new ProdutoDTO();
            dto.Marca = txtMarca.Text;
            dto.Preço = Convert.ToDecimal(txtPreco.Text);           
            dto.Fornecedor = fofo.Id;
            ProdutoBusiness bs = new ProdutoBusiness();
            bs.Salvar(dto);

            MessageBox.Show("Salvo com sucesso", "Sonhe alto", MessageBoxButtons.OK, MessageBoxIcon.Information); 


        }

        private void cboFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Close();
        }
    }
}
