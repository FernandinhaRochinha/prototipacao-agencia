﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.DB
{
   public  class FuncionarioDTO
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Departamento { get; set; }

        public string CPF { get; set; }

        public string RG { get; set; }

        public DateTime Nascimento { get; set; }

        public string Endereco { get; set; }

        public string Cep { get; set; }

        public string Telefone { get; set; }

        public string Emergencia { get; set; }

        public string Celular { get; set; }

        public string Usuario { get; set; }

        public string Senha { get; set; }



    }
}
