﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static frmLogin.DB.Funcionario.FuncionarioView;

namespace frmLogin.DB.Funcionario
{
    public class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script =
                @"insert into tb_funcionario
                (
                nm_funcionario,
                ds_cpf,
                ds_rg,
                dt_nascimento,
                ds_endereco,
                ds_cep,
                ds_telefone,
                ds_emergencia,
                ds_celular,
                nm_usuario,
                ds_senha
                )
                VALUES 
                (
                @nm_funcionario,
                @ds_cpf,
                @ds_rg,
                @dt_nascimento,
                @ds_endereco,
                @ds_cep,
                @ds_telefone,
                @ds_emergencia,
                @ds_celular,
                @nm_usuario,
                @ds_senha
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_emergencia", dto.Emergencia));
            parms.Add(new MySqlParameter("ds_celular", dto.Celular));
            parms.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public List<FuncionarioView> Listar()
        {
            string script = "select * from view_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioView> lista = new List<FuncionarioView>();

            while (reader.Read())

            {
                FuncionarioView dtos = new FuncionarioView();
                dtos.Id = reader.GetInt32("id_funcionario");
                dtos.Nome = reader.GetString("nm_funcionario");
                          
                dtos.Cpf = reader.GetString("ds_cpf");
                dtos.Rg = reader.GetString("ds_rg");
                dtos.Nascimento = reader.GetDateTime("dt_nascimento");
                dtos.Endereco = reader.GetString("ds_endereco");
                dtos.Cep = reader.GetString("ds_cep");
                dtos.Telefone = reader.GetInt32("ds_telefone");
                dtos.Emergencia = reader.GetString("ds_emergencia");
                dtos.Celular = reader.GetInt32("ds_celular");
                dtos.Usuario = reader.GetString("nm_usuario");

                dtos.Senha = reader.GetString("ds_senha");

                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }

        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(FuncionarioDTO dto)
        {
            string script = @"UPDATE tb_funcionario
                                SET 
                                    nm_funcionario = @nm_funcionario , 
                                    dt_nascimento = @dt_nascimento ,
                                    ds_cpf = @ds_cpf , 
                                    ds_rg = @ds_rg , 
                                    ds_cep = @ds_cep ,
                                    ds_endereco  =  @ds_endereco,
                                    ds_telefone  = @ds_telefone,
                                    ds_emergencia = @ds_emergencia,
                                    ds_celular = @ds_celular,
                                    nm_usuario = @nm_usuario,
                                    ds_senha = @ds_senha
                                WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id));
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome)); 
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_emergencia", dto.Emergencia));
            parms.Add(new MySqlParameter("ds_celular", dto.Celular));
            parms.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }
        public List<FuncionarioDTO> Consultar(FuncionarioDTO dto)
        {
            string script = "select * from tb_funcionario WHERE nm_funcionario = @nm_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();

            while (reader.Read())

            {
                FuncionarioDTO dtos = new FuncionarioDTO();
                dtos.Id = reader.GetInt32("id_funcionario");
                dtos.Nome = reader.GetString("nm_funcionario");
                dtos.CPF = reader.GetString("ds_cpf");
                dtos.RG = reader.GetString("ds_rg");
                dtos.Nascimento = reader.GetDateTime("dt_nascimento");
                dtos.Endereco = reader.GetString("ds_endereco");
                dtos.Cep = reader.GetString("ds_cep");
                dtos.Telefone = reader.GetString("ds_telefone");
                dtos.Emergencia = reader.GetString("ds_emergencia");
                dtos.Celular = reader.GetString("ds_celular");
                dtos.Usuario = reader.GetString("nm_usuario");
                dtos.Senha = reader.GetString("ds_senha");

                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }
    }
       

        
}
