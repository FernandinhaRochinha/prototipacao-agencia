﻿using frmLogin.DB;
using frmLogin.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Funcionario
{
    class FuncionarioBusiness
    {
        FuncionarioDatabase db = new FuncionarioDatabase();

        public int Salvar(FuncionarioDTO dto)
        {
            return db.Salvar(dto);
        }

        public List<FuncionarioView> Listar()
        {
            return db.Listar();
        }

        public void Remover (int id)
        {
            db.Remover(id);
        }

        public void Alterar (FuncionarioDTO dto)
        {
            db.Alterar(dto);
        }

        public List<FuncionarioDTO> Consultar(FuncionarioDTO dto)
        {
            return db.Consultar(dto);
        }

    }
}
