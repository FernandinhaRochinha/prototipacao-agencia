﻿using frmLogin.Acesso;
using frmLogin.DB;
using frmLogin.Funcionario;
using frmLogin.Funcionario_COMPLETO__;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Telas.Cadastros
{
    public partial class frmFuncionarioConsultar : Form
    {
        public frmFuncionarioConsultar()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Hide();

        }
        void Consultar()
        {
            FuncionarioDTO dto = new FuncionarioDTO();
            dto.Nome = txtNome.Text;

            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Consultar(dto);

            dgvFuncionario.AutoGenerateColumns = false;
            dgvFuncionario.DataSource = lista;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Consultar();
        }

        private void dgvFuncionario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 6)
            {
                FuncionarioDTO dto = dgvFuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;
                frmFuncionarioAlterar frm = new frmFuncionarioAlterar();
                frm.Loadscreen(dto);
                frm.Show();
                Hide();

            }

            if(e.ColumnIndex == 7)
            {
                FuncionarioDTO dto = dgvFuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;
                DialogResult resp = MessageBox.Show("Deseja realmente excluir?", "Viagem", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if(resp == DialogResult.Yes)
                {
                    AcessoBusiness acessoBusiness = new AcessoBusiness();
                    acessoBusiness.Remover(dto.Id);

                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(dto.Id);
                    MessageBox.Show("Funcionário Removido com sucesso!", "Viagem", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Consultar();
                }
            }
        }
    }
}
