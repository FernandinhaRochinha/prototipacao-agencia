﻿using frmLogin.Acesso;
using frmLogin.DB;
using frmLogin.Departamento;
using frmLogin.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Telas.Cadastros
{
    public partial class frmFuncionarioCadastrar : Form
    {
        public frmFuncionarioCadastrar()
        {
            InitializeComponent();
        }

        private void frmFuncionarioCadastrar_Load(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Hide();

        }

       

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FuncionarioDTO dto = new FuncionarioDTO();
            dto.Nome = txtNome.Text;
            dto.CPF = txtCPF.Text;
            dto.RG = txtRG.Text;
            dto.Nascimento = dtpNascimento.Value;
            dto.Endereco = txtEndereco.Text;
            dto.Cep = txtCEP.Text;
            dto.Telefone = txtTelefone.Text;
            dto.Emergencia = txtEmergencia.Text;
            dto.Celular = txtCelular.Text;
            dto.Usuario = txtUsuario.Text;
            dto.Senha = txtSenha.Text;

            FuncionarioBusiness bs = new FuncionarioBusiness();
            int idFuncionario = bs.Salvar(dto);

            MessageBox.Show("Salvo com sucesso!", "Viagem", MessageBoxButtons.OK, MessageBoxIcon.Information);

            // Salva Permissões na Tabela Nivel de Acesso

            //Checka Permissões de Vendas

            if (chkSalvarVendas.Checked == true || chkRemoverVendas.Checked == true || chkConsultarVendas.Checked == true || chkAlterarVendas.Checked == true)
            {

                DepartamentoDTO deteo = new DepartamentoDTO();
                deteo.Departamento = "Vendas";
                // Pesquisa departamento e volta o id

                DepartamentoDatabase departamento = new DepartamentoDatabase();
                int id_departamento = departamento.Consultar(deteo);

                // Passa Valores pro DTO

                AcessoDTO dt = new AcessoDTO();
                dt.IdFuncionario = idFuncionario;
                dt.IdDepartamento = id_departamento;
                dt.Salvar = chkSalvarVendas.Checked;
                dt.Remover = chkRemoverVendas.Checked;
                dt.Consultar = chkConsultarVendas.Checked;
                dt.Alterar = chkAlterarVendas.Checked;

                AcessoBusiness acesso = new AcessoBusiness();
                acesso.Salvar(dt);

            }
            //Checka permissões em Funcionários
            if (chkSalvarFuncionario.Checked == true || chkRemoverFuncionario.Checked == true || chkConsultarFuncionario.Checked == true || chkAlterarFuncionario.Checked == true)
            {

                DepartamentoDTO deteo = new DepartamentoDTO();
                deteo.Departamento = "Funcionários";
                // Pesquisa departamento e volta o id

                DepartamentoDatabase departamento = new DepartamentoDatabase();
                int id_departamento = departamento.Consultar(deteo);

                // Passa Valores pro DTO

                AcessoDTO dt = new AcessoDTO();
                dt.IdFuncionario = idFuncionario;
                dt.IdDepartamento = id_departamento;
                dt.Salvar = chkSalvarFuncionario.Checked;
                dt.Remover = chkRemoverFuncionario.Checked;
                dt.Consultar = chkConsultarFuncionario.Checked;
                dt.Alterar = chkAlterarFuncionario.Checked;

                AcessoBusiness acesso = new AcessoBusiness();
                acesso.Salvar(dt);

            }
            //Checka Permissões de RH
            if (chkSalvarRH.Checked == true || chkRemoverRH.Checked == true || chkConsultarRH.Checked == true || chkAlterarRH.Checked == true)
            {

                DepartamentoDTO deteo = new DepartamentoDTO();
                deteo.Departamento = "RH";
                // Pesquisa departamento e volta o id

                DepartamentoDatabase departamento = new DepartamentoDatabase();
                int id_departamento = departamento.Consultar(deteo);

                // Passa Valores pro DTO

                AcessoDTO dt = new AcessoDTO();
                dt.IdFuncionario = idFuncionario;
                dt.IdDepartamento = id_departamento;
                dt.Salvar = chkSalvarRH.Checked;
                dt.Remover = chkRemoverRH.Checked;
                dt.Consultar = chkConsultarRH.Checked;
                dt.Alterar = chkAlterarRH.Checked;

                AcessoBusiness acesso = new AcessoBusiness();
                acesso.Salvar(dt);

            }
            //Checka Permissões do Financeiro
            if (chkSalvarFinanceiro.Checked == true || chkRemoverFinanceiro.Checked == true || chkConsultarFinanceiro.Checked == true || chkAlterarFinanceiro.Checked == true)
            {

                DepartamentoDTO deteo = new DepartamentoDTO();
                deteo.Departamento = "Financeiro";
                // Pesquisa departamento e volta o id

                DepartamentoDatabase departamento = new DepartamentoDatabase();
                int id_departamento = departamento.Consultar(deteo);

                // Passa Valores pro DTO

                AcessoDTO dt = new AcessoDTO();
                dt.IdFuncionario = idFuncionario;
                dt.IdDepartamento = id_departamento;
                dt.Salvar = chkSalvarFinanceiro.Checked;
                dt.Remover = chkRemoverFinanceiro.Checked;
                dt.Consultar = chkConsultarFinanceiro.Checked;
                dt.Alterar = chkAlterarFinanceiro.Checked;

                AcessoBusiness acesso = new AcessoBusiness();
                acesso.Salvar(dt);

            }
            //Checka Permissões de Compras
            if (chkSalvarCompras.Checked == true || chkRemoverCompras.Checked == true || chkConsultarCompras.Checked == true || chkAlterarCompras.Checked == true)
            {

                DepartamentoDTO deteo = new DepartamentoDTO();
                deteo.Departamento = "Compras";
                // Pesquisa departamento e volta o id

                DepartamentoDatabase departamento = new DepartamentoDatabase();
                int id_departamento = departamento.Consultar(deteo);

                // Passa Valores pro DTO

                AcessoDTO dt = new AcessoDTO();
                dt.IdFuncionario = idFuncionario;
                dt.IdDepartamento = id_departamento;
                dt.Salvar = chkSalvarCompras.Checked;
                dt.Remover = chkRemoverCompras.Checked;
                dt.Consultar = chkConsultarCompras.Checked;
                dt.Alterar = chkAlterarCompras.Checked;

                AcessoBusiness acesso = new AcessoBusiness();
                acesso.Salvar(dt);

            }
            //Checka Permissões de Estoque
            if (chkSalvarEstoque.Checked == true || chkRemoverEstoque.Checked == true || chkConsultarEstoque.Checked == true || chkAlterarEstoque.Checked == true)
            {

                DepartamentoDTO deteo = new DepartamentoDTO();
                deteo.Departamento = "Estoque";
                // Pesquisa departamento e volta o id

                DepartamentoDatabase departamento = new DepartamentoDatabase();
                int id_departamento = departamento.Consultar(deteo);

                // Passa Valores pro DTO

                AcessoDTO dt = new AcessoDTO();
                dt.IdFuncionario = idFuncionario;
                dt.IdDepartamento = id_departamento;
                dt.Salvar = chkSalvarEstoque.Checked;
                dt.Remover = chkRemoverEstoque.Checked;
                dt.Consultar = chkConsultarEstoque.Checked;
                dt.Alterar = chkAlterarEstoque.Checked;

                AcessoBusiness acesso = new AcessoBusiness();
                acesso.Salvar(dt);


            }

            frmInicial tela = new frmInicial();
            tela.Show();
            Hide();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}
