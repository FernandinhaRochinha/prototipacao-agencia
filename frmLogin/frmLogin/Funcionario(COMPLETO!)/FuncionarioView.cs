﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.DB.Funcionario
{
            public  class FuncionarioView
            { 
        
             public int Id { get; set; }

             public string Nome { get; set; }

             public string Departamento { get; set; }

             public string Usuario { get; set; }

             public string Senha { get; set; }

             public string Cpf { get; set; }
    
             public string Rg { get; set; }

             public DateTime Nascimento { get; set; }

             public string Endereco { get; set; }

             public string Cep { get; set; }

             public int Telefone { get; set; }

             public string Emergencia { get; set; }

             public int Celular { get; set; }
            }    
}
