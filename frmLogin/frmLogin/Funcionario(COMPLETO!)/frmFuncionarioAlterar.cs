﻿using frmLogin.DB;
using frmLogin.Funcionario;
using frmLogin.Telas.Cadastros;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Funcionario_COMPLETO__
{
    public partial class frmFuncionarioAlterar : Form
    {
        public frmFuncionarioAlterar()
        {
            InitializeComponent();
        }

        FuncionarioDTO dto;



        public void Loadscreen(FuncionarioDTO dto)
        {
            this.dto = dto;
            txtNome.Text = dto.Nome;
            txtCPF.Text = dto.CPF;
            txtRG.Text = dto.RG;
            dtpNascimento.Value = dto.Nascimento;
            txtEndereco.Text = dto.Endereco;
            txtCEP.Text = dto.Cep;
            txtTelefone.Text = dto.Telefone;
            txtCelular.Text = dto.Celular;
            txtEmergencia.Text = dto.Emergencia;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            dto.Nome = txtNome.Text;
            dto.CPF = txtCPF.Text;
            dto.RG = txtRG.Text;
            dto.Nascimento = dtpNascimento.Value;
            dto.Endereco = txtEndereco.Text;
            dto.Cep = txtCEP.Text;
            dto.Telefone = txtTelefone.Text;
            dto.Celular = txtCelular.Text;
            dto.Emergencia = txtEmergencia.Text;
            FuncionarioBusiness business = new FuncionarioBusiness();
            business.Alterar(dto);

            MessageBox.Show("Alterado com sucesso", "Viagem", MessageBoxButtons.OK, MessageBoxIcon.Information);

            frmFuncionarioConsultar frm = new frmFuncionarioConsultar();
            frm.Show();
            Hide();

        }

        private void frmFuncionarioAlterar_Load(object sender, EventArgs e)
        {

        }
    }
}
