﻿namespace frmLogin.Funcionario_COMPLETO__
{
    partial class frmFuncionarioAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFuncionarioAlterar));
            this.chkConsultarCompras = new System.Windows.Forms.CheckBox();
            this.chkConsultarEstoque = new System.Windows.Forms.CheckBox();
            this.chkSalvarEstoque = new System.Windows.Forms.CheckBox();
            this.chkAlterarEstoque = new System.Windows.Forms.CheckBox();
            this.chkAlterarCompras = new System.Windows.Forms.CheckBox();
            this.chkRemoverCompras = new System.Windows.Forms.CheckBox();
            this.gpbFinanceiro = new System.Windows.Forms.GroupBox();
            this.chkConsultarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkSalvarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkAlterarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkRemoverFinanceiro = new System.Windows.Forms.CheckBox();
            this.gpbRH = new System.Windows.Forms.GroupBox();
            this.chkConsultarRH = new System.Windows.Forms.CheckBox();
            this.chkSalvarRH = new System.Windows.Forms.CheckBox();
            this.chkAlterarRH = new System.Windows.Forms.CheckBox();
            this.chkRemoverRH = new System.Windows.Forms.CheckBox();
            this.gpbFuncionario = new System.Windows.Forms.GroupBox();
            this.chkConsultarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkSalvarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkAlterarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkRemoverFuncionario = new System.Windows.Forms.CheckBox();
            this.gpbVendas = new System.Windows.Forms.GroupBox();
            this.chkConsultarVendas = new System.Windows.Forms.CheckBox();
            this.chkSalvarVendas = new System.Windows.Forms.CheckBox();
            this.chkAlterarVendas = new System.Windows.Forms.CheckBox();
            this.chkRemoverVendas = new System.Windows.Forms.CheckBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.chkSalvarCompras = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkRemoverEstoque = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtpNascimento = new System.Windows.Forms.DateTimePicker();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtCEP = new System.Windows.Forms.MaskedTextBox();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.txtEmergencia = new System.Windows.Forms.TextBox();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.gpbEstoque = new System.Windows.Forms.GroupBox();
            this.gpbCompras = new System.Windows.Forms.GroupBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.gpbFinanceiro.SuspendLayout();
            this.gpbRH.SuspendLayout();
            this.gpbFuncionario.SuspendLayout();
            this.gpbVendas.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.gpbEstoque.SuspendLayout();
            this.gpbCompras.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkConsultarCompras
            // 
            this.chkConsultarCompras.AutoSize = true;
            this.chkConsultarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarCompras.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarCompras.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarCompras.Name = "chkConsultarCompras";
            this.chkConsultarCompras.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarCompras.TabIndex = 41;
            this.chkConsultarCompras.Text = "Consultar";
            this.chkConsultarCompras.UseVisualStyleBackColor = true;
            // 
            // chkConsultarEstoque
            // 
            this.chkConsultarEstoque.AutoSize = true;
            this.chkConsultarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarEstoque.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarEstoque.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarEstoque.Name = "chkConsultarEstoque";
            this.chkConsultarEstoque.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarEstoque.TabIndex = 41;
            this.chkConsultarEstoque.Text = "Consultar";
            this.chkConsultarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkSalvarEstoque
            // 
            this.chkSalvarEstoque.AutoSize = true;
            this.chkSalvarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarEstoque.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarEstoque.Location = new System.Drawing.Point(92, 25);
            this.chkSalvarEstoque.Name = "chkSalvarEstoque";
            this.chkSalvarEstoque.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarEstoque.TabIndex = 42;
            this.chkSalvarEstoque.Text = "Salvar";
            this.chkSalvarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkAlterarEstoque
            // 
            this.chkAlterarEstoque.AutoSize = true;
            this.chkAlterarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarEstoque.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarEstoque.Location = new System.Drawing.Point(6, 54);
            this.chkAlterarEstoque.Name = "chkAlterarEstoque";
            this.chkAlterarEstoque.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarEstoque.TabIndex = 43;
            this.chkAlterarEstoque.Text = "Alterar";
            this.chkAlterarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkAlterarCompras
            // 
            this.chkAlterarCompras.AutoSize = true;
            this.chkAlterarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarCompras.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarCompras.Location = new System.Drawing.Point(6, 54);
            this.chkAlterarCompras.Name = "chkAlterarCompras";
            this.chkAlterarCompras.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarCompras.TabIndex = 43;
            this.chkAlterarCompras.Text = "Alterar";
            this.chkAlterarCompras.UseVisualStyleBackColor = true;
            // 
            // chkRemoverCompras
            // 
            this.chkRemoverCompras.AutoSize = true;
            this.chkRemoverCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverCompras.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverCompras.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverCompras.Location = new System.Drawing.Point(92, 54);
            this.chkRemoverCompras.Name = "chkRemoverCompras";
            this.chkRemoverCompras.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverCompras.TabIndex = 44;
            this.chkRemoverCompras.Text = "Remover";
            this.chkRemoverCompras.UseVisualStyleBackColor = true;
            // 
            // gpbFinanceiro
            // 
            this.gpbFinanceiro.Controls.Add(this.chkConsultarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkSalvarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkAlterarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkRemoverFinanceiro);
            this.gpbFinanceiro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbFinanceiro.Location = new System.Drawing.Point(14, 207);
            this.gpbFinanceiro.Name = "gpbFinanceiro";
            this.gpbFinanceiro.Size = new System.Drawing.Size(164, 97);
            this.gpbFinanceiro.TabIndex = 71;
            this.gpbFinanceiro.TabStop = false;
            this.gpbFinanceiro.Text = "Financeiro";
            // 
            // chkConsultarFinanceiro
            // 
            this.chkConsultarFinanceiro.AutoSize = true;
            this.chkConsultarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarFinanceiro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarFinanceiro.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarFinanceiro.Name = "chkConsultarFinanceiro";
            this.chkConsultarFinanceiro.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarFinanceiro.TabIndex = 41;
            this.chkConsultarFinanceiro.Text = "Consultar";
            this.chkConsultarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkSalvarFinanceiro
            // 
            this.chkSalvarFinanceiro.AutoSize = true;
            this.chkSalvarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarFinanceiro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarFinanceiro.Location = new System.Drawing.Point(92, 25);
            this.chkSalvarFinanceiro.Name = "chkSalvarFinanceiro";
            this.chkSalvarFinanceiro.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarFinanceiro.TabIndex = 42;
            this.chkSalvarFinanceiro.Text = "Salvar";
            this.chkSalvarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkAlterarFinanceiro
            // 
            this.chkAlterarFinanceiro.AutoSize = true;
            this.chkAlterarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarFinanceiro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarFinanceiro.Location = new System.Drawing.Point(6, 54);
            this.chkAlterarFinanceiro.Name = "chkAlterarFinanceiro";
            this.chkAlterarFinanceiro.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarFinanceiro.TabIndex = 43;
            this.chkAlterarFinanceiro.Text = "Alterar";
            this.chkAlterarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkRemoverFinanceiro
            // 
            this.chkRemoverFinanceiro.AutoSize = true;
            this.chkRemoverFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverFinanceiro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverFinanceiro.Location = new System.Drawing.Point(92, 54);
            this.chkRemoverFinanceiro.Name = "chkRemoverFinanceiro";
            this.chkRemoverFinanceiro.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverFinanceiro.TabIndex = 44;
            this.chkRemoverFinanceiro.Text = "Remover";
            this.chkRemoverFinanceiro.UseVisualStyleBackColor = true;
            // 
            // gpbRH
            // 
            this.gpbRH.Controls.Add(this.chkConsultarRH);
            this.gpbRH.Controls.Add(this.chkSalvarRH);
            this.gpbRH.Controls.Add(this.chkAlterarRH);
            this.gpbRH.Controls.Add(this.chkRemoverRH);
            this.gpbRH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbRH.Location = new System.Drawing.Point(14, 310);
            this.gpbRH.Name = "gpbRH";
            this.gpbRH.Size = new System.Drawing.Size(166, 93);
            this.gpbRH.TabIndex = 70;
            this.gpbRH.TabStop = false;
            this.gpbRH.Text = "RH";
            // 
            // chkConsultarRH
            // 
            this.chkConsultarRH.AutoSize = true;
            this.chkConsultarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarRH.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarRH.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarRH.Location = new System.Drawing.Point(5, 25);
            this.chkConsultarRH.Name = "chkConsultarRH";
            this.chkConsultarRH.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarRH.TabIndex = 41;
            this.chkConsultarRH.Text = "Consultar";
            this.chkConsultarRH.UseVisualStyleBackColor = true;
            // 
            // chkSalvarRH
            // 
            this.chkSalvarRH.AutoSize = true;
            this.chkSalvarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarRH.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarRH.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarRH.Location = new System.Drawing.Point(91, 25);
            this.chkSalvarRH.Name = "chkSalvarRH";
            this.chkSalvarRH.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarRH.TabIndex = 42;
            this.chkSalvarRH.Text = "Salvar";
            this.chkSalvarRH.UseVisualStyleBackColor = true;
            // 
            // chkAlterarRH
            // 
            this.chkAlterarRH.AutoSize = true;
            this.chkAlterarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarRH.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarRH.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarRH.Location = new System.Drawing.Point(5, 54);
            this.chkAlterarRH.Name = "chkAlterarRH";
            this.chkAlterarRH.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarRH.TabIndex = 43;
            this.chkAlterarRH.Text = "Alterar";
            this.chkAlterarRH.UseVisualStyleBackColor = true;
            // 
            // chkRemoverRH
            // 
            this.chkRemoverRH.AutoSize = true;
            this.chkRemoverRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverRH.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverRH.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverRH.Location = new System.Drawing.Point(91, 54);
            this.chkRemoverRH.Name = "chkRemoverRH";
            this.chkRemoverRH.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverRH.TabIndex = 44;
            this.chkRemoverRH.Text = "Remover";
            this.chkRemoverRH.UseVisualStyleBackColor = true;
            // 
            // gpbFuncionario
            // 
            this.gpbFuncionario.Controls.Add(this.chkConsultarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkSalvarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkAlterarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkRemoverFuncionario);
            this.gpbFuncionario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbFuncionario.Location = new System.Drawing.Point(189, 108);
            this.gpbFuncionario.Name = "gpbFuncionario";
            this.gpbFuncionario.Size = new System.Drawing.Size(161, 93);
            this.gpbFuncionario.TabIndex = 69;
            this.gpbFuncionario.TabStop = false;
            this.gpbFuncionario.Text = "Funcionario";
            // 
            // chkConsultarFuncionario
            // 
            this.chkConsultarFuncionario.AutoSize = true;
            this.chkConsultarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarFuncionario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarFuncionario.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarFuncionario.Name = "chkConsultarFuncionario";
            this.chkConsultarFuncionario.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarFuncionario.TabIndex = 41;
            this.chkConsultarFuncionario.Text = "Consultar";
            this.chkConsultarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkSalvarFuncionario
            // 
            this.chkSalvarFuncionario.AutoSize = true;
            this.chkSalvarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarFuncionario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarFuncionario.Location = new System.Drawing.Point(92, 25);
            this.chkSalvarFuncionario.Name = "chkSalvarFuncionario";
            this.chkSalvarFuncionario.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarFuncionario.TabIndex = 42;
            this.chkSalvarFuncionario.Text = "Salvar";
            this.chkSalvarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkAlterarFuncionario
            // 
            this.chkAlterarFuncionario.AutoSize = true;
            this.chkAlterarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarFuncionario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarFuncionario.Location = new System.Drawing.Point(6, 54);
            this.chkAlterarFuncionario.Name = "chkAlterarFuncionario";
            this.chkAlterarFuncionario.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarFuncionario.TabIndex = 43;
            this.chkAlterarFuncionario.Text = "Alterar";
            this.chkAlterarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkRemoverFuncionario
            // 
            this.chkRemoverFuncionario.AutoSize = true;
            this.chkRemoverFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverFuncionario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverFuncionario.Location = new System.Drawing.Point(92, 54);
            this.chkRemoverFuncionario.Name = "chkRemoverFuncionario";
            this.chkRemoverFuncionario.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverFuncionario.TabIndex = 44;
            this.chkRemoverFuncionario.Text = "Remover";
            this.chkRemoverFuncionario.UseVisualStyleBackColor = true;
            // 
            // gpbVendas
            // 
            this.gpbVendas.Controls.Add(this.chkConsultarVendas);
            this.gpbVendas.Controls.Add(this.chkSalvarVendas);
            this.gpbVendas.Controls.Add(this.chkAlterarVendas);
            this.gpbVendas.Controls.Add(this.chkRemoverVendas);
            this.gpbVendas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbVendas.Location = new System.Drawing.Point(14, 108);
            this.gpbVendas.Name = "gpbVendas";
            this.gpbVendas.Size = new System.Drawing.Size(163, 93);
            this.gpbVendas.TabIndex = 68;
            this.gpbVendas.TabStop = false;
            this.gpbVendas.Text = "Vendas";
            // 
            // chkConsultarVendas
            // 
            this.chkConsultarVendas.AutoSize = true;
            this.chkConsultarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarVendas.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarVendas.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarVendas.Name = "chkConsultarVendas";
            this.chkConsultarVendas.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarVendas.TabIndex = 41;
            this.chkConsultarVendas.Text = "Consultar";
            this.chkConsultarVendas.UseVisualStyleBackColor = true;
            // 
            // chkSalvarVendas
            // 
            this.chkSalvarVendas.AutoSize = true;
            this.chkSalvarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarVendas.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarVendas.Location = new System.Drawing.Point(92, 25);
            this.chkSalvarVendas.Name = "chkSalvarVendas";
            this.chkSalvarVendas.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarVendas.TabIndex = 42;
            this.chkSalvarVendas.Text = "Salvar";
            this.chkSalvarVendas.UseVisualStyleBackColor = true;
            // 
            // chkAlterarVendas
            // 
            this.chkAlterarVendas.AutoSize = true;
            this.chkAlterarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarVendas.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarVendas.Location = new System.Drawing.Point(6, 54);
            this.chkAlterarVendas.Name = "chkAlterarVendas";
            this.chkAlterarVendas.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarVendas.TabIndex = 43;
            this.chkAlterarVendas.Text = "Alterar";
            this.chkAlterarVendas.UseVisualStyleBackColor = true;
            // 
            // chkRemoverVendas
            // 
            this.chkRemoverVendas.AutoSize = true;
            this.chkRemoverVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverVendas.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverVendas.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverVendas.Location = new System.Drawing.Point(92, 54);
            this.chkRemoverVendas.Name = "chkRemoverVendas";
            this.chkRemoverVendas.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverVendas.TabIndex = 44;
            this.chkRemoverVendas.Text = "Remover";
            this.chkRemoverVendas.UseVisualStyleBackColor = true;
            // 
            // txtSenha
            // 
            this.txtSenha.Location = new System.Drawing.Point(86, 52);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.PasswordChar = '*';
            this.txtSenha.Size = new System.Drawing.Size(221, 20);
            this.txtSenha.TabIndex = 67;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(22, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 19);
            this.label13.TabIndex = 66;
            this.label13.Text = "Senha:";
            // 
            // chkSalvarCompras
            // 
            this.chkSalvarCompras.AutoSize = true;
            this.chkSalvarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarCompras.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarCompras.Location = new System.Drawing.Point(92, 25);
            this.chkSalvarCompras.Name = "chkSalvarCompras";
            this.chkSalvarCompras.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarCompras.TabIndex = 42;
            this.chkSalvarCompras.Text = "Salvar";
            this.chkSalvarCompras.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 19);
            this.label5.TabIndex = 64;
            this.label5.Text = "Usuario:";
            // 
            // chkRemoverEstoque
            // 
            this.chkRemoverEstoque.AutoSize = true;
            this.chkRemoverEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverEstoque.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverEstoque.Location = new System.Drawing.Point(92, 54);
            this.chkRemoverEstoque.Name = "chkRemoverEstoque";
            this.chkRemoverEstoque.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverEstoque.TabIndex = 44;
            this.chkRemoverEstoque.Text = "Remover";
            this.chkRemoverEstoque.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(-2, -2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(400, 510);
            this.tabControl1.TabIndex = 38;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtpNascimento);
            this.tabPage1.Controls.Add(this.btnVoltar);
            this.tabPage1.Controls.Add(this.txtCelular);
            this.tabPage1.Controls.Add(this.txtTelefone);
            this.tabPage1.Controls.Add(this.txtCEP);
            this.tabPage1.Controls.Add(this.txtRG);
            this.tabPage1.Controls.Add(this.txtCPF);
            this.tabPage1.Controls.Add(this.txtEmergencia);
            this.tabPage1.Controls.Add(this.txtEndereco);
            this.tabPage1.Controls.Add(this.txtNome);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(392, 484);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Funcionário";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtpNascimento
            // 
            this.dtpNascimento.Location = new System.Drawing.Point(173, 166);
            this.dtpNascimento.Name = "dtpNascimento";
            this.dtpNascimento.Size = new System.Drawing.Size(139, 20);
            this.dtpNascimento.TabIndex = 60;
            // 
            // btnVoltar
            // 
            this.btnVoltar.Font = new System.Drawing.Font("Tw Cen MT", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.Location = new System.Drawing.Point(30, 425);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(30, 23);
            this.btnVoltar.TabIndex = 59;
            this.btnVoltar.Text = "<";
            this.btnVoltar.UseVisualStyleBackColor = true;
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(96, 271);
            this.txtCelular.Mask = "+00 (00) 00000-0000";
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(216, 20);
            this.txtCelular.TabIndex = 57;
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(108, 245);
            this.txtTelefone.Mask = "+00 (00) 0000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(204, 20);
            this.txtTelefone.TabIndex = 56;
            // 
            // txtCEP
            // 
            this.txtCEP.Location = new System.Drawing.Point(73, 219);
            this.txtCEP.Mask = "00000-000";
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(239, 20);
            this.txtCEP.TabIndex = 55;
            // 
            // txtRG
            // 
            this.txtRG.Location = new System.Drawing.Point(73, 134);
            this.txtRG.Mask = "00.000.000.-00";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(239, 20);
            this.txtRG.TabIndex = 54;
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(74, 108);
            this.txtCPF.Mask = "000000000/00";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(238, 20);
            this.txtCPF.TabIndex = 53;
            // 
            // txtEmergencia
            // 
            this.txtEmergencia.Location = new System.Drawing.Point(124, 297);
            this.txtEmergencia.Name = "txtEmergencia";
            this.txtEmergencia.Size = new System.Drawing.Size(188, 20);
            this.txtEmergencia.TabIndex = 52;
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(109, 194);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(203, 20);
            this.txtEndereco.TabIndex = 50;
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(90, 77);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(224, 20);
            this.txtNome.TabIndex = 49;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tw Cen MT", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(23, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(355, 37);
            this.label12.TabIndex = 47;
            this.label12.Text = "ALTERAR FUNCIONÁRIO";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(26, 220);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 19);
            this.label11.TabIndex = 46;
            this.label11.Text = "CEP :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(26, 193);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 19);
            this.label10.TabIndex = 45;
            this.label10.Text = "Endereço :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(24, 242);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 19);
            this.label9.TabIndex = 44;
            this.label9.Text = "Telefone :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(26, 272);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 19);
            this.label8.TabIndex = 43;
            this.label8.Text = "Celular :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 294);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 19);
            this.label7.TabIndex = 42;
            this.label7.Text = "Emergência :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tw Cen MT", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(27, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 17);
            this.label4.TabIndex = 40;
            this.label4.Text = "RG :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tw Cen MT", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(25, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 17);
            this.label3.TabIndex = 39;
            this.label3.Text = "Data de Nascimento :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tw Cen MT", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(27, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 17);
            this.label2.TabIndex = 38;
            this.label2.Text = "CPF :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 19);
            this.label1.TabIndex = 37;
            this.label1.Text = "Nome :";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.gpbEstoque);
            this.tabPage2.Controls.Add(this.gpbCompras);
            this.tabPage2.Controls.Add(this.gpbFinanceiro);
            this.tabPage2.Controls.Add(this.gpbRH);
            this.tabPage2.Controls.Add(this.gpbFuncionario);
            this.tabPage2.Controls.Add(this.gpbVendas);
            this.tabPage2.Controls.Add(this.txtSenha);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.txtUsuario);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(392, 484);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Permissões";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Tw Cen MT", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(132, 418);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 35);
            this.button1.TabIndex = 74;
            this.button1.Text = "Alterar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // gpbEstoque
            // 
            this.gpbEstoque.Controls.Add(this.chkConsultarEstoque);
            this.gpbEstoque.Controls.Add(this.chkSalvarEstoque);
            this.gpbEstoque.Controls.Add(this.chkAlterarEstoque);
            this.gpbEstoque.Controls.Add(this.chkRemoverEstoque);
            this.gpbEstoque.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbEstoque.Location = new System.Drawing.Point(189, 310);
            this.gpbEstoque.Name = "gpbEstoque";
            this.gpbEstoque.Size = new System.Drawing.Size(166, 93);
            this.gpbEstoque.TabIndex = 73;
            this.gpbEstoque.TabStop = false;
            this.gpbEstoque.Text = "Estoque";
            // 
            // gpbCompras
            // 
            this.gpbCompras.Controls.Add(this.chkConsultarCompras);
            this.gpbCompras.Controls.Add(this.chkSalvarCompras);
            this.gpbCompras.Controls.Add(this.chkAlterarCompras);
            this.gpbCompras.Controls.Add(this.chkRemoverCompras);
            this.gpbCompras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbCompras.Location = new System.Drawing.Point(189, 207);
            this.gpbCompras.Name = "gpbCompras";
            this.gpbCompras.Size = new System.Drawing.Size(161, 97);
            this.gpbCompras.TabIndex = 72;
            this.gpbCompras.TabStop = false;
            this.gpbCompras.Text = "Compras";
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(86, 26);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(221, 20);
            this.txtUsuario.TabIndex = 65;
            // 
            // frmFuncionarioAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.BackgroundImage = global::frmLogin.Properties.Resources.shutterstock_617106911_imagem_1180x485px;
            this.ClientSize = new System.Drawing.Size(397, 507);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmFuncionarioAlterar";
            this.Text = "frmFuncionarioAlterar";
            this.Load += new System.EventHandler(this.frmFuncionarioAlterar_Load);
            this.gpbFinanceiro.ResumeLayout(false);
            this.gpbFinanceiro.PerformLayout();
            this.gpbRH.ResumeLayout(false);
            this.gpbRH.PerformLayout();
            this.gpbFuncionario.ResumeLayout(false);
            this.gpbFuncionario.PerformLayout();
            this.gpbVendas.ResumeLayout(false);
            this.gpbVendas.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.gpbEstoque.ResumeLayout(false);
            this.gpbEstoque.PerformLayout();
            this.gpbCompras.ResumeLayout(false);
            this.gpbCompras.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkConsultarCompras;
        private System.Windows.Forms.CheckBox chkConsultarEstoque;
        private System.Windows.Forms.CheckBox chkSalvarEstoque;
        private System.Windows.Forms.CheckBox chkAlterarEstoque;
        private System.Windows.Forms.CheckBox chkAlterarCompras;
        private System.Windows.Forms.CheckBox chkRemoverCompras;
        private System.Windows.Forms.GroupBox gpbFinanceiro;
        private System.Windows.Forms.CheckBox chkConsultarFinanceiro;
        private System.Windows.Forms.CheckBox chkSalvarFinanceiro;
        private System.Windows.Forms.CheckBox chkAlterarFinanceiro;
        private System.Windows.Forms.CheckBox chkRemoverFinanceiro;
        private System.Windows.Forms.GroupBox gpbRH;
        private System.Windows.Forms.CheckBox chkConsultarRH;
        private System.Windows.Forms.CheckBox chkSalvarRH;
        private System.Windows.Forms.CheckBox chkAlterarRH;
        private System.Windows.Forms.CheckBox chkRemoverRH;
        private System.Windows.Forms.GroupBox gpbFuncionario;
        private System.Windows.Forms.CheckBox chkConsultarFuncionario;
        private System.Windows.Forms.CheckBox chkSalvarFuncionario;
        private System.Windows.Forms.CheckBox chkAlterarFuncionario;
        private System.Windows.Forms.CheckBox chkRemoverFuncionario;
        private System.Windows.Forms.GroupBox gpbVendas;
        private System.Windows.Forms.CheckBox chkConsultarVendas;
        private System.Windows.Forms.CheckBox chkSalvarVendas;
        private System.Windows.Forms.CheckBox chkAlterarVendas;
        private System.Windows.Forms.CheckBox chkRemoverVendas;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkSalvarCompras;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkRemoverEstoque;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DateTimePicker dtpNascimento;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.MaskedTextBox txtCelular;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.MaskedTextBox txtCEP;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.TextBox txtEmergencia;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox gpbEstoque;
        private System.Windows.Forms.GroupBox gpbCompras;
        private System.Windows.Forms.TextBox txtUsuario;
    }
}