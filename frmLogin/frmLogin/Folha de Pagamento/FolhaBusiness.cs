﻿using frmLogin.FluxodeCaixa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Folha_de_Pagamento
{
    public class FolhaBusiness
    {
        FolhaDatabase db = new FolhaDatabase();
        public int Salvar(FolhaDTO dto)
        {
            if (dto.Bt_Valetransporte == true)
            {
                if (dto.VT == 0)
                {
                    throw new ArgumentException("Vale Transporte é obrigatório", "Américas Mecânica");
                }
            }
            return db.Salvar(dto);
        }
       
        public void Remover(int Id)
        {
            db.Remover(Id);
        }
        public List<FolhaDTO> Buscar(FolhaDTO NomeFuncionario)
        {
            return db.Buscar(NomeFuncionario);
        }

    }
}
