﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Folha_de_Pagamento
{
    class Calculo
    {
        
        
            public decimal CalcularValorPorHora(decimal salario, int HorasTrabalhadas)
            {
                decimal resposta = salario / HorasTrabalhadas;

                return resposta;
            }

            public decimal CalcularValorHoraExtra(decimal ValorDaHora, decimal Porcentagem, int horasextras)
            {
                decimal Horaextra = (ValorDaHora * (Porcentagem / 100)) + ValorDaHora;

                Horaextra = horasextras * Horaextra;

                return Horaextra;
            }

            public int CalcularDiasTrabalhados(int Faltas1semana, int faltas2semana, int Faltas3semana, int Faltas4semana)
            {
                if (Faltas1semana > 0)
                {
                    Faltas1semana = Faltas1semana + 1;
                }
                if (faltas2semana > 0)
                {
                    faltas2semana = faltas2semana + 1;
                }
                if (Faltas3semana > 0)
                {
                    Faltas3semana = Faltas3semana + 1;
                }
                if (Faltas4semana > 0)
                {
                    Faltas4semana = Faltas4semana + 1;
                }


                int totaldefaltas = Faltas1semana + faltas2semana + Faltas3semana + Faltas4semana;
                int diastrabalhados = 26 - totaldefaltas;

                return diastrabalhados;
            }

            public int CalcularFinaisdeSemanas(int Faltas1semana, int faltas2semana, int Faltas3semana, int Faltas4semana)
            {
                int finaisdesemanaperdido = 0;


                if (Faltas1semana == 0)
                {
                    finaisdesemanaperdido = finaisdesemanaperdido + 1;
                }
                if (faltas2semana == 0)
                {
                    finaisdesemanaperdido = finaisdesemanaperdido + 1;
                }
                if (Faltas3semana == 0)
                {
                    finaisdesemanaperdido = finaisdesemanaperdido + 1;
                }
                if (Faltas4semana == 0)
                {
                    finaisdesemanaperdido = finaisdesemanaperdido + 1;
                }

                return finaisdesemanaperdido;
            }

            public decimal CalcularDSR(decimal TotalHorasExtras, int DiasTrabalhados, int FinaisDeSemana)
            {
                decimal DSR = TotalHorasExtras / DiasTrabalhados * FinaisDeSemana;

                return DSR;
            }

            public decimal CalcularINSS(decimal salario, decimal TotalSalarioBruto)
            {
                decimal PorcentagemINSS = 0;
                if (salario <= 1693.72m)
                {
                    PorcentagemINSS = 8;
                }
                if (salario >= 1693.73m && salario <= 2822.90m)
                {
                    PorcentagemINSS = 9;
                }
                if (salario >= 2822.91m && salario <= 5645.80m)
                {
                    PorcentagemINSS = 11;
                }

                decimal INSS = TotalSalarioBruto * (PorcentagemINSS / 100);

                return INSS;
            }

            public decimal CalcularIR(decimal totalsalario, decimal ValorINSS)
            {
                decimal PorcentagemIR = 0;
                decimal valordodesconto = 0;

                totalsalario = totalsalario - ValorINSS;

                if (totalsalario >= 1903.99m && totalsalario <= 2826.65m)
                {
                    PorcentagemIR = 7.5m;
                    valordodesconto = 142.80m;
                }
                if (totalsalario >= 2826.66m && totalsalario <= 3751.05m)
                {
                    PorcentagemIR = 15;
                    valordodesconto = 354.80m;
                }
                if (totalsalario >= 3751.06m && totalsalario <= 4664.68m)
                {
                    PorcentagemIR = 22.5m;
                    valordodesconto = 636.13m;
                }
                if (totalsalario > 4664.69m)
                {
                    PorcentagemIR = 27.5m;
                    valordodesconto = 869.36m;
                }

                decimal valordescontado = totalsalario * (PorcentagemIR / 100);
                decimal IR = valordescontado - valordodesconto;

                return IR;
            }

            public decimal CalcularFGTS(decimal totalsalario)
            {
                decimal FGTS = totalsalario * 0.08m;
                return FGTS;
            }

            public int CalcularTotalDeFaltas(int Faltas1semana, int faltas2semana, int Faltas3semana, int Faltas4semana)
            {
                int finaisdesemanaperdido = 0;


                if (Faltas1semana > 0)
                {
                    finaisdesemanaperdido = finaisdesemanaperdido + 1;
                }
                if (faltas2semana > 0)
                {
                    finaisdesemanaperdido = finaisdesemanaperdido + 1;
                }
                if (Faltas3semana > 0)
                {
                    finaisdesemanaperdido = finaisdesemanaperdido + 1;
                }
                if (Faltas4semana > 0)
                {
                    finaisdesemanaperdido = finaisdesemanaperdido + 1;
                }

                int totalfaltas = Faltas1semana + faltas2semana + Faltas3semana + Faltas4semana + finaisdesemanaperdido;
                return totalfaltas;

            }

            public decimal ValordeDesconto(decimal totalsalario, decimal valorinss)
            {
                totalsalario = totalsalario - valorinss;
                decimal valordedesconto = 0;
                if (totalsalario >= 1903.99m && totalsalario <= 2826.65m)
                {
                    valordedesconto = 142.80m;
                }
                if (totalsalario >= 2826.66m && totalsalario <= 3751.05m)
                {
                    valordedesconto = 354.80m;
                }
                if (totalsalario >= 3751.06m && totalsalario <= 4664.68m)
                {
                    valordedesconto = 636.13m;
                }
                if (totalsalario > 4664.69m)
                {
                    valordedesconto = 869.36m;
                }
                return valordedesconto;
            }
        

    }
}
