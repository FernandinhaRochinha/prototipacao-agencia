﻿using frmLogin.Base;
using frmLogin.Folha_de_Pagamento;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.FluxodeCaixa
{
     public class FolhaDatabase
    {
        Database db = new Database();
        public int Salvar(FolhaDTO dto)
        {
            string script = @"INSERT INTO tb_folhadepagamento(nm_horasextras, bt_valetransporte, vl_inss, vl_ir, vl_fgts, vl_valetransporte, vl_salarioliquido, id_funcionario,dt_pagamento) 
                        VALUES(@nm_horasextras, @bt_valetransporte, @vl_inss, @vl_ir, @vl_fgts, @vl_valetransporte, @vl_salarioliquido, @id_funcionario,@dt_pagamento)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_horasextras", dto.HorasExtras));
            parms.Add(new MySqlParameter("bt_valetransporte", dto.Bt_Valetransporte));
            parms.Add(new MySqlParameter("vl_inss", dto.INSS));
            parms.Add(new MySqlParameter("vl_ir", dto.IR));
            parms.Add(new MySqlParameter("vl_fgts", dto.FGTS));
            parms.Add(new MySqlParameter("vl_valetransporte", dto.VT));
            parms.Add(new MySqlParameter("vl_salarioliquido", dto.SalarioLiquido));
            parms.Add(new MySqlParameter("id_funcionario", dto.IdFuncionario));
            parms.Add(new MySqlParameter("dt_pagamento", dto.Data));


            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<FolhaDTO> Buscar(int NomeFuncionario)
        {
            string script = @"
                    SELECT tb_folhadepagamento.id_folhadepagamento, tb_folhadepagamento.nm_horasextras, tb_folhadepagamento.vl_inss, tb_folhadepagamento.vl_ir,
                    tb_folhadepagamento.vl_fgts, tb_folhadepagamento.vl_valetransporte, tb_folhadepagamento.vl_salarioliquido, tb_funcionario.id_funcionario, tb_funcionario.nm_funcionario
                      from tb_folhadepagamento
                       JOIN tb_funcionario ON tb_funcionario.id_funcionario = tb_folhadepagamento.id_funcionario
                    WHERE tb_funcionario.nm_funcionario like @nm_funcionario;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", "%" + NomeFuncionario + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FolhaDTO> lis = new List<FolhaDTO>();

            while (reader.Read())
            {
                FolhaDTO dto = new FolhaDTO();
                dto.Id = reader.GetInt32("id_folhadepagamento");
                dto.HorasExtras = reader.GetInt32("nm_horasextras");
                dto.INSS = reader.GetDecimal("vl_inss");
                dto.IR = reader.GetDecimal("vl_ir");
                dto.FGTS = reader.GetDecimal("vl_fgts");
                dto.VT = reader.GetDecimal("vl_ValeTransporte");
                dto.SalarioLiquido = reader.GetDecimal("vl_salarioliquido");
                dto.IdFuncionario = reader.GetInt32("id_funcionario");
                dto.NomeFuncionario = reader.GetString("nm_funcionario");
                dto.Data = reader.GetDateTime("dt_pagamento");


                lis.Add(dto);

            }
            return lis;
        }
        public void Alterar(FolhaDTO dto)
        {
            string script = @"UPDATE INTO tb_folhadePagamento
                            set 
                    nm_horasextras = @nm_horasextras,
                    bt_valetransporte = @bt_valetransporte, 
                    vl_inss = @vl_inss,            
                    vl_ir = @vl_ir ,
                    vl_fgts = @vl_fgts, 
                    vl_valetransporte = @vl_valetransporte ,
                    vl_salarioliquido = @vl_salarioliquido , 
                    id_funcionario = @id_funcionario 
                    WHERE id_folhadepagamento = @id_folhadepagamento";



            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_folhadepagamento", dto.Id));
            parms.Add(new MySqlParameter("nm_horasextras", dto.HorasExtras));
            parms.Add(new MySqlParameter("bt_valetransporte", dto.Bt_Valetransporte));
            parms.Add(new MySqlParameter("vl_inss", dto.INSS));
            parms.Add(new MySqlParameter("vl_ir", dto.IR));
            parms.Add(new MySqlParameter("vl_fgts", dto.FGTS));
            parms.Add(new MySqlParameter("vl_valetransporte", dto.VT));
            parms.Add(new MySqlParameter("vl_salarioliquido", dto.SalarioLiquido));

            
            db.ExecuteInsertScript(script, parms);

        }


        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_folhadepagamento WHERE id_folhadepagamento = @id_folhadepagamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_folhadepagamento", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    
}
}
