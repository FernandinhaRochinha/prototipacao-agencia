﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Folha_de_Pagamento
{
    public partial class FolhaConsultar : Form
    {
        public FolhaConsultar()
        {
            InitializeComponent();
        }
        void Buscar()
        {
            FolhaDTO dto = new FolhaDTO();
            dto.NomeFuncionario = txtFuncionario.Text;

            FolhaBusiness business = new FolhaBusiness();
            List<FolhaDTO> lista = business.Buscar(dto);

            dgvFolhaPagamento.AutoGenerateColumns = false;
            dgvFolhaPagamento.DataSource = lista;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

        }
    }
}

        