﻿using frmLogin.DB;
using frmLogin.DB.Funcionario;
using frmLogin.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Folha_de_Pagamento
{
    public partial class FolhaCadastrar : Form
    {
        public FolhaCadastrar()
        {
            InitializeComponent();
            maskedTextBox1.Visible = true;

            label13.Visible = false;
            numericUpDown3.Visible = false;
            label14.Visible = false;
            numericUpDown2.Visible = false;
            CarregarCombo();

        }
        decimal ValordeDesconto;
        void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioView> Funcionarios = business.Listar();

            comboBox2.ValueMember = nameof(FuncionarioView.Id);
            comboBox2.DisplayMember = nameof(FuncionarioView.Nome);
            comboBox2.DataSource = Funcionarios;
            textBox2.Text = nameof(FuncionarioView.Departamento);

        }
        Calculo folha = new Calculo();
        decimal VT = 0;
        decimal SalarioBruto = 0;
        decimal DSR;
        decimal INSS;
        decimal IR;
        decimal FGTS;
        decimal SalarioLiquido;

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int ht = Convert.ToInt32(numericUpDown1.Value);
                int he = Convert.ToInt32(numericUpDown2.Value);
                decimal porcentagem = numericUpDown3.Value;
                int diastrabalhados = folha.CalcularDiasTrabalhados(Convert.ToInt32(textBox3.Text), Convert.ToInt32(textBox4.Text), Convert.ToInt32(textBox5.Text), Convert.ToInt32(textBox6.Text));
                int finaisdesemana = folha.CalcularFinaisdeSemanas(Convert.ToInt32(textBox3.Text), Convert.ToInt32(textBox4.Text), Convert.ToInt32(textBox5.Text), Convert.ToInt32(textBox6.Text));
                int totaldefaltas = folha.CalcularTotalDeFaltas(Convert.ToInt32(textBox3.Text), Convert.ToInt32(textBox4.Text), Convert.ToInt32(textBox5.Text), Convert.ToInt32(textBox6.Text));

                


                if (numericUpDown2.Value > 0 && checkBox1.Checked == true)
                {

                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);
                    decimal totalhorasextras = folha.CalcularValorHoraExtra(valorporhora, porcentagem, he);
                    DSR = folha.CalcularDSR(totalhorasextras, diastrabalhados, finaisdesemana);
                    decimal TotalSalarioBruto = SalarioBruto + totalhorasextras + DSR;



                    INSS = folha.CalcularINSS(SalarioBruto, TotalSalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(TotalSalarioBruto, INSS);
                    IR = folha.CalcularIR(TotalSalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(TotalSalarioBruto);
                    VT = SalarioBruto * 0.06m;

                    SalarioLiquido = SalarioBruto + totalhorasextras + DSR - INSS - ValordeDesconto - VT;

                    lblSalario.Visible = true;
                    maskedTextBox1.Visible = true;
                    maskedTextBox1.Text = SalarioLiquido.ToString();

                }
                else if (numericUpDown2.Value > 0 && checkBox1.Checked == false)
                {
                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);
                    decimal totalhorasextras = folha.CalcularValorHoraExtra(valorporhora, porcentagem, he);
                    DSR = folha.CalcularDSR(totalhorasextras, diastrabalhados, finaisdesemana);
                    decimal TotalSalarioBruto = SalarioBruto + totalhorasextras + DSR;



                    INSS = folha.CalcularINSS(SalarioBruto, TotalSalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(TotalSalarioBruto, INSS);

                    IR = folha.CalcularIR(TotalSalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(TotalSalarioBruto);


                    decimal SalarioLiquido = SalarioBruto + totalhorasextras + DSR - INSS - ValordeDesconto - VT;

                    lblSalario.Visible = true;
                    maskedTextBox1.Visible = true;
                    maskedTextBox1.Text = SalarioLiquido.ToString();
                }
                else if (numericUpDown2.Value == 0 && checkBox1.Checked == true)
                {

                    decimal descontodosalariobruto = SalarioBruto / 30 * totaldefaltas;
                    SalarioBruto = SalarioBruto - descontodosalariobruto;

                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);

                    INSS = folha.CalcularINSS(SalarioBruto, SalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(SalarioBruto, SalarioBruto);

                    IR = folha.CalcularIR(SalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(SalarioBruto);
                    VT = SalarioBruto * 0.06m;

                    SalarioLiquido = SalarioBruto - INSS - ValordeDesconto - VT;

                    lblSalario.Visible = true;
                    maskedTextBox1.Visible = true;
                    maskedTextBox1.Text = SalarioLiquido.ToString();
                }

                else if (numericUpDown2.Value == 0 && checkBox1.Checked == false)
                {
                    decimal descontodosalariobruto = SalarioBruto / 30 * totaldefaltas;
                    SalarioBruto = SalarioBruto - descontodosalariobruto;

                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);

                    INSS = folha.CalcularINSS(SalarioBruto, SalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(SalarioBruto, SalarioBruto);

                    IR = folha.CalcularIR(SalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(SalarioBruto);

                    SalarioLiquido = SalarioBruto - INSS - ValordeDesconto - VT;

                    lblSalario.Visible = true;
                    maskedTextBox1.Visible = true;
                    maskedTextBox1.Text = SalarioLiquido.ToString();
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Preencha todos os campos", " Viagem!");

            }



        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

            if (checkBox1.Checked == true)
            {
                label13.Visible = true;
                numericUpDown3.Visible = true;
                label14.Visible = true;
                numericUpDown2.Visible = true;
            }

            if (checkBox1.Checked == false)
            {
                label13.Visible = false;
                numericUpDown3.Visible = false;
                label14.Visible = false;
                numericUpDown2.Visible = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                FolhaDTO dto = new FolhaDTO();
                 FuncionarioDTO = comboBox2.SelectedItem as FuncionarioView;

                dto.Bt_Valetransporte = checkBox2.Checked;
                dto.FGTS = FGTS;
                dto.HorasExtras = Convert.ToInt32(numericUpDown2.Value);
                dto.INSS = INSS;
                dto.IR = IR;
                dto.SalarioLiquido = SalarioLiquido;
                dto.VT = VT;
               
                dto.Data = DateTime.Now;

                FolhaBusiness business = new FolhaBusiness();
                business.Salvar(dto);
                MessageBox.Show("Folha de pagamento salva com sucesso", "Codinomes Bar");

            }
            catch (Exception)
            {

                MessageBox.Show("Preencha os campos em branco", "Codinomes Bar :");
            }
        }
    }
}


