﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Folha_de_Pagamento
{
   public  class FolhaDTO
    {
        
            public int Id { get; set; }

            public int HorasExtras { get; set; }

            public decimal INSS { get; set; }

            public decimal IR { get; set; }

            public decimal FGTS { get; set; }

            public bool Bt_Valetransporte { get; set; }

            public decimal VT { get; set; }

            public decimal SalarioLiquido { get; set; }

            public int IdFuncionario { get; set; }

            public string NomeFuncionario { get; set; }

            public DateTime Data { get; set; }

        
    }
}
