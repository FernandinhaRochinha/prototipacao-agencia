﻿using frmLogin.Base;
using frmLogin.Fornecedor;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Estoque
{
    public class EstoqueDatabase
    {
        Database db = new Database();
        public int Salvar(EstoqueDTO dto)
        {
            string script = @"INSERT INTO tb_estoque    
                  (
                    dt_datavalidade,
                    vl_quantidade,
                    fk_produto
                                                      
                    )
                    VALUES
                    (   
                    @dt_datavalidade,
                    @vl_quantidade,
                    @fk_produto
                  
                    )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_datavalidade", dto.Validade));
            parms.Add(new MySqlParameter("vl_quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("fk_produto", dto.Id_Produto));
            
          
            return db.ExecuteInsertScriptWithPk(script, parms);


        }

        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_estoque WHERE id_estoque = @id_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", ID));


            db.ExecuteInsertScript(script, parms);
        }

        public List<EstoqueDTO> Listar()
        {
            string script = "select * from tb_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();


            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> lista = new List<EstoqueDTO>();

            while (reader.Read())

            {
                EstoqueDTO dtos = new EstoqueDTO();
                dtos.ID = reader.GetInt32("id_estoque");
                dtos.Quantidade = reader.GetInt32("vl_quantidade");
                dtos.Validade = reader.GetDateTime("dt_datavalidade");
                dtos.Id_Produto = reader.GetInt32("fk_produto");
               
                lista.Add(dtos);
            }
            reader.Close();
            return lista;


        }

        public void Alterar(EstoqueDTO dto)
        {
            string script = @"UPDATE tb_estoque
                                SET 
                                    dt_datavalidade = @dt_datavalidade, 
                                    vl_quantidade = @vl_quantidade,
                                    fk_produto = @fk_produto
                                   
                               
                                WHERE id_estoque = @id_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_datavalidade", dto.Validade));
            parms.Add(new MySqlParameter("vl_quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("fk_produto", dto.Id_Produto));


            db.ExecuteInsertScript(script, parms);

        }

        public List<EstoqueDTO> Consultar(EstoqueDTO dto)
        {
            string script = "select * from tb_estoque WHERE dt_datavalidade like @dt_datavalidade";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_datavalidade", "%" + dto.Validade + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> lista = new List<EstoqueDTO>();

            while (reader.Read())

            {
                EstoqueDTO dtos = new EstoqueDTO();
                dtos.ID = reader.GetInt32("id_estoque");
                dtos.Quantidade = reader.GetInt32("vl_quantidade");
                dtos.Validade = reader.GetDateTime("dt_datavalidade");
                dtos.Id_Produto = reader.GetInt32("fk_produto");

                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }
    }
}
