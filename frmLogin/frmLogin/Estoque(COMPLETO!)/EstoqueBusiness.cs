﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Estoque
{
    public class EstoqueBusiness
    {
        EstoqueDatabase db = new EstoqueDatabase();

        public int Salvar(EstoqueDTO dto)
        {
            return db.Salvar(dto);

        }

        public List<EstoqueDTO> Consultar(EstoqueDTO dto)
        {
            return db.Consultar(dto);
        }
        public void Remover(int ID)
        {
            db.Remover(ID);
        }
        public void Alterar (EstoqueDTO dto)
        {
            db.Alterar(dto);

        }
        public List<EstoqueDTO> Listar()
        {
           return db.Listar();
        }
    }
}
