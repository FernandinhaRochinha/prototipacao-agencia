﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Departamento
{
   public class DepartamentoDatabase
    {
        Database db = new Database();
        public int Consultar(DepartamentoDTO dto)
        {
            string script =
                @"SELECT * FROM tb_departamento WHERE nm_departamento = @nm_departamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_departamento", dto.Departamento));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            int id = 0;
           while(reader.Read())
            {
                DepartamentoDTO dt = new DepartamentoDTO();
                dt.Id = reader.GetInt32("id_departamento");
                id = dt.Id;
            }
            return id;
        }
    }
}
