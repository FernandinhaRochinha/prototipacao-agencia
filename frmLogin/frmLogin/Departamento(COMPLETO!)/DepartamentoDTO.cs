﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Departamento
{
    public class DepartamentoDTO
    {
        public int Id { get; set; }

        public string Departamento { get; set; }
    }
}
