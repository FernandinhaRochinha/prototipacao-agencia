﻿using frmLogin.Compras;
using frmLogin.Fornecedor;
using frmLogin.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Compras_COMPLETO__
{
    public partial class ComprasAlterar : Form
    {
        public ComprasAlterar()
        {
            InitializeComponent();
        }

        ComprasDTO dtos;
        private void ComprasAlterar_Load(object sender, EventArgs e)
        {

        }

        public void LoadScreen(ComprasDTO dto)
        {
            this.dtos = dto;
            cboProduto.ValueMember = dto.Produto.ToString();
            cboFornecedor.Text = dto.Fornecedor.ToString();
            (txtQuantidade.Text) = dto.Quantidade.ToString();
            txtTotal.Text = dto.Total.ToString();
            dtpCompra.Text = dto.DatadaCompra.ToLongDateString();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            dtos.Produto = Convert.ToInt32(cboProduto.SelectedValue);
            dtos.Fornecedor = Convert.ToInt32(cboFornecedor.SelectedValue);
            dtos.Quantidade =Convert.ToInt32( txtQuantidade.Text);
            dtos.Total =Convert.ToDecimal( txtTotal.Text);
            dtos.DatadaCompra =Convert.ToDateTime( dtpCompra.Text);

            FornecedorBusiness bs = new FornecedorBusiness();
            //bs.Alterar(dtos);

            MessageBox.Show("Alterado com sucesso", "Viagem", MessageBoxButtons.OK, MessageBoxIcon.Information);

            frmFornecedorConsultar frm = new frmFornecedorConsultar();
            frm.Show();
            Hide();
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        void CarregarCombo()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.Id);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Fornecedor);

            cboFornecedor.DataSource = lista;


            ProdutoBusiness businesss = new Produto.ProdutoBusiness();
            List<ProdutoDTO> listas = businesss.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.ID);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Marca);

            cboProduto.DataSource = lista;


        }
    }
}
