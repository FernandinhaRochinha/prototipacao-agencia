﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Compras
{
    public class ComprasDTO
    {
        public int ID { get; set; }

        public int Produto { get; set; }

        public int Fornecedor { get; set; }

        public int Quantidade { get; set; }

        public decimal Total { get; set; }

        public DateTime DatadaCompra { get; set; }


    }
}
