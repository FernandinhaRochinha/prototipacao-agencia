﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Compras
{
    class ComprasBusiness
    {
        ComprasDatabase db = new ComprasDatabase();

        public int Salvar(ComprasDTO dto)
        {
            return db.Salvar(dto);
        }
        public void Remover(int ID)
        {
            db.Remover(ID);

        }

        public List<ComprasDTO> Listar()
        {
            return db.Listar();
        }
        public void Alterar(ComprasDTO dto)
        {
            db.Alterar(dto);
        }
        public List<ComprasDTO> Consultar(ComprasDTO dto)
        {
            return db.Consultar(dto);
        }
    }
}
