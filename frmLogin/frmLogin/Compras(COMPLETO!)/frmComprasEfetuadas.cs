﻿using frmLogin.Compras_COMPLETO__;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Compras
{
    public partial class frmComprasEfetuadas : Form
    {
        public frmComprasEfetuadas()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        void Consultar()
        {
            ComprasDTO dto = new ComprasDTO();
            dto.Produto =Convert.ToInt32( txtnome.Text);

            ComprasBusiness business = new ComprasBusiness();
            List<ComprasDTO> lista = business.Consultar(dto);

            dgvCompra.AutoGenerateColumns = false;
            dgvCompra.DataSource = lista;
        }

        private void dgvCompra_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                ComprasDTO dto = dgvCompra.CurrentRow.DataBoundItem as ComprasDTO;
                DialogResult resp = MessageBox.Show("Deseja realmente excluir?", "Viagem", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (resp == DialogResult.Yes)
                {



                    ComprasBusiness business = new ComprasBusiness();
                    business.Remover(dto.ID);
                    MessageBox.Show("Compra removida com sucesso!", "Viagem", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Consultar();

                }
                if (e.ColumnIndex == 6)
                {
                    ComprasDTO dto1 = dgvCompra.CurrentRow.DataBoundItem as ComprasDTO;
                    ComprasAlterar tela = new ComprasAlterar();
                    tela.LoadScreen(dto);
                    tela.Show();
                    Hide();
                }
            }
        }

        private void frmComprasEfetuadas_Load(object sender, EventArgs e)
        {

        }
    }
}

