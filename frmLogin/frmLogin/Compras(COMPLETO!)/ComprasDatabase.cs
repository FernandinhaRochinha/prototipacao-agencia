﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Compras
{
    class ComprasDatabase
    {
        public int Salvar(ComprasDTO dto)
        {
            string script = @"INSERT INTO tb_compras (fk_produto, fk_fornecedor, vl_quantidade, vl_total
                            dt_diadacompra) values (@fk_produto, @fk_fornecedor, @vl_quantidade, @vl_total
                            @dt_diadecompra)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_produto", dto.Produto));
            parms.Add(new MySqlParameter("fk_fornecedor", dto.Fornecedor));
            parms.Add(new MySqlParameter("vl_quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("vl_total", dto.Total));
            parms.Add(new MySqlParameter("dt_diadacompra", dto.DatadaCompra));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_compras WHERE id_compras = @id_compras";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_compras", ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public List<ComprasDTO> Listar()
        {
            string script = "select * from tb_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ComprasDTO> lista = new List<ComprasDTO>();

            while (reader.Read())
            {
                ComprasDTO dto = new ComprasDTO();
                dto.ID = reader.GetInt32("id_compras");
                dto.Produto = reader.GetInt32("fk_produto");
                dto.Fornecedor = reader.GetInt32("fk_fornecedor");
                dto.Quantidade = reader.GetInt32("vl_quantidade");
                dto.Total = reader.GetDecimal("vl_total");
                dto.DatadaCompra = reader.GetDateTime("dt_diadacompra");


                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
        public void Alterar(ComprasDTO dto)
        {
            string script = @"UPDATE tb_compras
                                SET id_compras = @id_compras,
                                    fk_produto = @fk_produto , 
                                    fk_fornecedor = @fk_fornecedor ,
                                    vl_quantidade = @vl_quantidade , 
                                    vl_total = @vl_total , 
                                    dt_diadacompra = @dt_diadacompra ,  
                                    
                                   
                                    
                                WHERE id_compras = @id_compras";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_compras", dto.ID));
            parms.Add(new MySqlParameter("fk_produto", dto.Produto));
            parms.Add(new MySqlParameter("fk_fornecedor", dto.Fornecedor));
            parms.Add(new MySqlParameter("vl_quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("vl_total", dto.Total));
            parms.Add(new MySqlParameter("dt_diadacompra", dto.DatadaCompra));
         



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
          }
        public List<ComprasDTO> Consultar(ComprasDTO dto)
        {
            string script = "select * from tb_compras WHERE fk_produto like @fk_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_produto", "%" + dto.Produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ComprasDTO> lista = new List<ComprasDTO>();

            while (reader.Read())

            {
                ComprasDTO dtoz = new ComprasDTO();
                dtoz.ID = reader.GetInt32("id_compras");
                dtoz.Produto = reader.GetInt32("fk_produto");
                dtoz.Fornecedor = reader.GetInt32("fk_fornecedor");
                dtoz.Quantidade = reader.GetInt32("vl_quantidade");
                dtoz.Total = reader.GetDecimal("vl_total");
                dtoz.DatadaCompra = reader.GetDateTime("dt_diadacompra");

                lista.Add(dtoz);
            }
            reader.Close();
            return lista;
        }

    }

   }
