﻿using frmLogin.Fornecedor;
using frmLogin.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Compras
{
    public partial class frmCompras : Form
    {
        public frmCompras()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

            FornecedorBusiness lista = new FornecedorBusiness();
            List<FornecedorDTO> listar = lista.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.Id);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Fornecedor);
            cboFornecedor.DataSource = listar;
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProdutoBusiness lista = new ProdutoBusiness();
            List<ProdutoDTO> listar = lista.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.ID);
            cboFornecedor.DisplayMember = nameof(ProdutoDTO.Marca);
            cboFornecedor.DataSource = listar;

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Hide();

        }

        private void frmCompras_Load(object sender, EventArgs e)
        {
           
        }
        void CarregarCombo()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.Id);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Fornecedor);

            cboFornecedor.DataSource = lista;


            ProdutoBusiness businesss = new Produto.ProdutoBusiness();
            List<ProdutoDTO> listas = businesss.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.ID);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Marca);

            cboProduto.DataSource = lista;


        }

    }
}
