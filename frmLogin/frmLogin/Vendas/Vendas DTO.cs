﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Vendas
{
    class Vendas_DTO
    { 
        public int Id { get; set; }

        public Boolean Idaevolta { get; set; }

        public DateTime Ida { get; set; }

        public DateTime Volta { get; set; }

        public int Adulto { get; set; }

        public int Crianca { get; set; }

        public string Origem { get; set; }

        public string Destino { get; set; }

        public DateTime Horadaviagem { get; set; }

        public DateTime Datadaviagem { get; set; }

        public string Pagamento { get; set; }

    }
}
