﻿using frmLogin.Acesso;
using frmLogin.Fornecedor_COMPLETO__;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Fornecedor
{
    public partial class frmFornecedorConsultar : Form
    {
        public frmFornecedorConsultar()
        {
            InitializeComponent();
            Consultar();
        }

        
        void Consultar()
        {
            FornecedorDTO dto = new FornecedorDTO();
            dto.Fornecedor = txtnome.Text;

            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Consultar(dto);

            dgvFornecedor.AutoGenerateColumns = false;
            dgvFornecedor.DataSource = lista;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                FornecedorDTO dto = dgvFornecedor.CurrentRow.DataBoundItem as FornecedorDTO;
                DialogResult resp = MessageBox.Show("Deseja realmente excluir?", "Viagem", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (resp == DialogResult.Yes)
                {
                    AcessoBusiness acessoBusiness = new AcessoBusiness();
                    acessoBusiness.Remover(dto.Id);

                    FornecedorBusiness business = new FornecedorBusiness();
                    business.Remover(dto.Id);
                    MessageBox.Show("Fornecedor Removido com sucesso!", "Viagem", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Consultar();

                }
            }
            if (e.ColumnIndex == 6)
            {
                FornecedorDTO dto = dgvFornecedor.CurrentRow.DataBoundItem as FornecedorDTO;
                frmFornecedorAlterar tela = new frmFornecedorAlterar();
                tela.LoadScreen(dto);
                tela.Show();
                Hide();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Consultar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Close();
        }

        private void frmFornecedorConsultar_Load(object sender, EventArgs e)
        {

        }
    }
}
