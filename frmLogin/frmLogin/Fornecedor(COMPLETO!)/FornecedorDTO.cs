﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Fornecedor
{
    public class FornecedorDTO
    {
        public int Id { get; set; }

        public string Fornecedor { get; set; }

        public string Telefone { get; set; }

        public string Numero { get; set; }  

        public string Endereco{ get; set; }

        public string CEP { get; set; }





    }
}
