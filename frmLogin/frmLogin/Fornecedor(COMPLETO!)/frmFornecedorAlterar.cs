﻿using frmLogin.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Fornecedor_COMPLETO__
{
    public partial class frmFornecedorAlterar : Form
    {
        public frmFornecedorAlterar()
        {
            InitializeComponent();
        }

        FornecedorDTO dtos;

        public void LoadScreen(FornecedorDTO dto)
        {
            this.dtos = dto;
            txtNumero.Text = dto.Numero;
            txtNome.Text = dto.Fornecedor ;
            txtEndereco.Text = dto.Endereco;
            txtCEP.Text = dto.CEP;
            txtTelefone.Text = dto.Telefone;
             

        }

        private void button1_Click(object sender, EventArgs e)
        {
            dtos.Fornecedor = txtNome.Text;
            dtos.Numero = txtNumero.Text;
            dtos.Telefone = txtTelefone.Text;
            dtos.Endereco = txtEndereco.Text;
            dtos.CEP = txtCEP.Text;

            FornecedorBusiness bs = new FornecedorBusiness();
            bs.Alterar(dtos);

            MessageBox.Show("Alterado com sucesso", "Viagem", MessageBoxButtons.OK, MessageBoxIcon.Information);

            frmFornecedorConsultar frm = new frmFornecedorConsultar();
            frm.Show();
            Hide();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void frmFornecedorAlterar_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmInicial TELA = new frmInicial();
            TELA.Show();
            Hide();
        }
    }
}
