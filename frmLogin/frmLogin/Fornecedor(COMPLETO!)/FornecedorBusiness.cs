﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Fornecedor
{
    public class FornecedorBusiness
    {
        FornecedorDataBase db = new FornecedorDataBase();

        public int Salvar(FornecedorDTO dto)
        {
            return db.Salvar(dto);
        }
        public void Remover(int ID)
        {
            db.Remover(ID);

        }

        public List<FornecedorDTO> Listar()
        {
            return db.Listar();
        }
        public void Alterar(FornecedorDTO dto)
        {
            db.Alterar(dto);
        }
        public List<FornecedorDTO> Consultar(FornecedorDTO dto)
        {
            return db.Consultar(dto);
        }
    }

}
