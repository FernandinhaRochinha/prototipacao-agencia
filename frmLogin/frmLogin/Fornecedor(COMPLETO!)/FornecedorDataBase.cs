﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Fornecedor
{
    public class FornecedorDataBase
    {
        Database db = new Database();
        public int Salvar(FornecedorDTO dto)
        {
            string script = @"INSERT INTO tb_fornecedor
                  (
                    nm_fornecedor,
                    ds_telefone,
                    vl_numero,
                    ds_endereco,
                    ds_cep
                        
             
                    )
                    VALUES
                    (   
                    @nm_fornecedor,
                    @ds_telefone,
                    @vl_numero,
                    @ds_endereco,
                    @ds_cep
                  
                    )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", dto.Fornecedor));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("vl_numero", dto.Numero));
            parms.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));
          

          
            return db.ExecuteInsertScriptWithPk(script, parms);


        }

        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_fornecedor WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", ID));

          
            db.ExecuteInsertScript(script, parms);
        }

        public List<FornecedorDTO> Listar()
        {
            string script = "select * from tb_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();

            while (reader.Read())

            {
                FornecedorDTO dtos = new FornecedorDTO();
                dtos.Id = reader.GetInt32("id_fornecedor");
                dtos.Fornecedor = reader.GetString("nm_fornecedor");
                dtos.Telefone = reader.GetString("ds_telefone");
                dtos.Numero = reader.GetString("vl_numero");
               dtos.Endereco = reader.GetString("ds_endereco");
                dtos.CEP = reader.GetString("ds_cep");
               

                lista.Add(dtos);
            }
            reader.Close();
            return lista;


        }

        public void Alterar(FornecedorDTO dto)
        {
            string script = @"UPDATE tb_fornecedor
                                SET 
                                    nm_fornecedor = @nm_fornecedor, 
                                    ds_telefone = @ds_telefone,
                                    vl_numero = @vl_numero, 
                                    ds_endereco = @ds_endereco, 
                                    ds_cep = @ds_cep
                               
                                WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", dto.Id));
            parms.Add(new MySqlParameter("nm_fornecedor", dto.Fornecedor));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("vl_numero", dto.Numero));
            parms.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));
        
           



     
            db.ExecuteInsertScript(script, parms);

        }

        public List<FornecedorDTO> Consultar(FornecedorDTO dto)
        {
            string script = "select * from tb_fornecedor WHERE nm_fornecedor like @nm_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor" ,"%" + dto.Fornecedor + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();

            while (reader.Read())

            {
                FornecedorDTO dtos = new FornecedorDTO();
                dtos.Id = reader.GetInt32("id_fornecedor");
                dtos.Fornecedor = reader.GetString("nm_fornecedor");
                dtos.Telefone = reader.GetString("ds_telefone");
                dtos.Numero = reader.GetString("vl_numero");
                dtos.Endereco = reader.GetString("ds_endereco");
                dtos.CEP = reader.GetString("ds_cep");


                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }
    }
}
