﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Fornecedor
{
    public partial class frmFornecedorCadastro : Form
    {
        public frmFornecedorCadastro()
        {
            InitializeComponent();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Salvar();
        }

        public void Salvar()
        {
            FornecedorDTO dto = new FornecedorDTO();
            dto.Fornecedor = txtNome.Text;
            dto.Telefone = txtTelefone.Text;
            dto.CEP = txtCEP.Text;
            dto.Endereco = txtEndereco.Text;
            dto.Numero = txtNumero.Text;

            FornecedorBusiness b = new FornecedorBusiness();
            b.Salvar(dto);

            MessageBox.Show("Salvo com sucesso!", "Viagem", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Close();
        }
    }
}
