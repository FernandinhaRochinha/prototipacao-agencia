﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Acesso
{
    public class AcessoBusiness
    {
        AcessoDatabase db = new AcessoDatabase();
        public int Salvar(AcessoDTO dto)
        {
            return db.Salvar(dto);
        }

        public void Alterar(AcessoDTO dto)
        {
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
             db.Remover(id);
        }

        public List<AcessoDTO> Consultar(AcessoDTO dto)
        {
            return db.Consultar(dto);
        }
    }
}
