﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Acesso
{
    public class AcessoDTO
    {
        public int Id { get; set; }

        public int IdFuncionario { get; set; }

        public int IdDepartamento { get; set; }

        public bool Salvar { get; set; }

        public bool Remover { get; set; }

        public bool Alterar { get; set; }

        public bool Consultar { get; set; }
    }
}
