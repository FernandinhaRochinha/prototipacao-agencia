﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Acesso
{
   public class AcessoDatabase
    {
        Database db = new Database();

        public int Salvar(AcessoDTO dto)
        {
            string script =
           @"INSERT INTO tb_nivel_acesso
                (
                id_funcionario,
                id_departamento,
                bl_salvar,
                bl_remover,
                bl_alterar,
                bl_consultar
                )
            VALUES
                (
                @id_funcionario,
                @id_departamento,
                @bl_salvar,
                @bl_remover,
                @bl_alterar,
                @bl_consultar
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.IdFuncionario));
            parms.Add(new MySqlParameter("id_departamento", dto.IdDepartamento));
            parms.Add(new MySqlParameter("bl_salvar", dto.Salvar));
            parms.Add(new MySqlParameter("bl_remover", dto.Remover));
            parms.Add(new MySqlParameter("bl_alterar", dto.Alterar));
            parms.Add(new MySqlParameter("bl_consultar", dto.Consultar));
           
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Alterar(AcessoDTO dto)
        {
            string script =
                @"UPDATE tb_nivel_acesso SET 
                                             id_departamento = @id_departamento,
                                             bl_salvar = @bl_salvar,
                                             bl_remover = @bl_remover,
                                             bl_alterar = @bl_alterar,
                                             bl_consultar = @bl_consultar
                WHERE id_funcionario = @id_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("id_funcionario", dto.IdFuncionario));
            parms.Add(new MySqlParameter("id_departamento", dto.IdDepartamento));
            parms.Add(new MySqlParameter("bl_salvar", dto.Salvar));
            parms.Add(new MySqlParameter("bl_remover", dto.Remover));
            parms.Add(new MySqlParameter("bl_alterar", dto.Alterar));
            parms.Add(new MySqlParameter("bl_consultar", dto.Consultar));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int id)
        {
            string script =
                @"DELETE FROM tb_nivel_acesso WHERE id_funcionario = @id_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<AcessoDTO> Consultar(AcessoDTO dto)
        {
            string script =
                @"SELECT * FROM tb_nivel_acesso WHERE id_funcionario = @id_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.IdFuncionario));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<AcessoDTO> lista = new List<AcessoDTO>();
            while (reader.Read())
            {
                AcessoDTO dt = new AcessoDTO();
                dt.Id = reader.GetInt32("id_nivel_acesso");
                dt.IdFuncionario = reader.GetInt32("id_funcionario");
                dt.IdDepartamento = reader.GetInt32("id_departamento");
                dt.Salvar = reader.GetBoolean("bl_salvar");
                dt.Remover = reader.GetBoolean("bl_remover");
                dt.Alterar = reader.GetBoolean("bl_alterar");
                dt.Consultar = reader.GetBoolean("bl_consultar");
                lista.Add(dt);
            }
            reader.Close();
            return lista;
        }
    }
}
