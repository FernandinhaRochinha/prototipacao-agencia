﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Clientes
{
    class ClienteBusiness
    {
        ClienteDatabase db = new ClienteDatabase();

        public int Salvar(ClienteDTO dto)
        {
            return db.Salvar(dto);
        }
        public void Remover(int ID)
        {
            db.Remover(ID);

        }

        public List<ClienteDTO> Listar()
        {
            return db.Listar();
        }
        public void Alterar(ClienteDTO dto)
        {
            db.Alterar(dto);
        }
        public List<ClienteDTO> Consultar(ClienteDTO dto)
        {
            return db.Consultar(dto);
        }
    }
}
