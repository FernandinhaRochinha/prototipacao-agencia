﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Clientes
{
    public partial class frmClienteCadastrar : Form
    {
        public frmClienteCadastrar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            ClienteDTO dto = new ClienteDTO();
            dto.Nome = txtNome.Text;
            dto.RG = Convert.ToString(txtRG.Text);
            dto.CPF = Convert.ToString(txtCPF.Text);
            dto.Email = txtEmail.Text;
            dto.Passaporte = Convert.ToString(txtPassaporte.Text);
            dto.Telefone =Convert.ToString( txtTelefone.Text);

            MessageBox.Show("Salvo com sucesso");

            ClienteBusiness bs = new ClienteBusiness();
            bs.Salvar(dto);

            frmInicial tela = new frmInicial();
            tela.Show();
            Hide();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Hide();
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
