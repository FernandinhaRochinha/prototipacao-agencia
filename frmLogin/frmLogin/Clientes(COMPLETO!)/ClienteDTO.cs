﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Clientes
{
    class ClienteDTO
    {
        public int ID { get; set; }

        public string Nome { get; set; }

        public string RG { get; set; }

        public string CPF { get; set; }

        public string Email { get; set; }

        public string Passaporte { get; set; }

        public string Telefone { get; set; }
    }
}
