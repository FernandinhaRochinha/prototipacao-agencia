﻿using frmLogin.Acesso;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Clientes
{
    public partial class frmClienteConsultar : Form
    {
        public frmClienteConsultar()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {


            Consultar();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Hide();

        }
        void Consultar()
        {
            ClienteDTO dto = new ClienteDTO();
            dto.Nome = txtBuscar.Text;

            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> lista = business.Consultar(dto);

            dgvClientes.AutoGenerateColumns = false;
            dgvClientes.DataSource = lista;
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 8)
            {
                ClienteDTO dto = dgvClientes.CurrentRow.DataBoundItem as ClienteDTO;
                DialogResult resp = MessageBox.Show("Deseja realmente excluir?", "Viagem", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (resp == DialogResult.Yes)
                {
                    AcessoBusiness acessoBusiness = new AcessoBusiness();
                    acessoBusiness.Remover(dto.ID);

                    ClienteBusiness business = new ClienteBusiness();
                    business.Remover(dto.ID);
                    MessageBox.Show("Fornecedor Removido com sucesso!", "Viagem", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Consultar();

                }
            }

        }
    }
}

