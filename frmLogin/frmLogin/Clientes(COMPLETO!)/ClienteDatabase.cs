﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Clientes
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"insert into tb_cliente 
                            (nm_nome, ds_rg, ds_cpf, ds_email, ds_passaporte, ds_telefone) values
                           (@nm_nome, @ds_rg, @ds_cpf, @ds_email, @ds_passaporte, @ds_telefone)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_passaporte", dto.Passaporte));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public List<ClienteDTO> Listar()
        {
            string script = "select * from tb_cliente";
            List<MySqlParameter> parms = new List<MySqlParameter>();


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();

            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.ID = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_nome");
                dto.RG = reader.GetString("ds_rg");
                dto.CPF = reader.GetString("ds_cpf");
                dto.Email = reader.GetString("ds_email");
                dto.Passaporte = reader.GetString("ds_passaporte");
                dto.Telefone = reader.GetString("ds_telefone");




                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public void Alterar(ClienteDTO dto)
        {
            string script = @"UPDATE tb_cliente
                                SET id_cliente = @id_cliente,
                                    nm_nome = @nm_nome , 
                                    ds_rg = @ds_rg ,
                                    ds_cpf = @ds_cpf , 
                                    ds_email = @ds_email , 
                                    ds_passaporte = @ds_passaporte ,  
                                    ds_telefone = @ds_telefone ,
                                  
                                   
                                   
                                    
                                WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.ID));
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_passaporte", dto.Passaporte));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public List<ClienteDTO> Consultar(ClienteDTO dto)
        {
            string script = "select * from tb_clientes WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", "%" + dto.Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();

            while (reader.Read())

            {
                ClienteDTO dtos = new ClienteDTO();
                dto.ID = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_nome");
                dto.RG = reader.GetString("ds_rg");
                dto.CPF = reader.GetString("ds_cpf");
                dto.Email = reader.GetString("ds_email");
                dto.Passaporte = reader.GetString("ds_passaporte");
                dto.Telefone = reader.GetString("ds_telefone");


                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }
    }
}