﻿using frmLogin.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Login_COMPLETO__
{
    public partial class CadastrarUsuario : Form
    {
        public CadastrarUsuario()
        {
            InitializeComponent();
        }

        private void txt_Click(object sender, EventArgs e)
        {
            LoginDTO dto = new LoginDTO();
            dto.Usuario = txtusuario.Text;
            dto.Senha = txtsenha.Text;

            LoginBusiness bs = new LoginBusiness();
            bs.Salvar(dto);

            MessageBox.Show("Salvo com sucesso!");

            frmLogin tela = new frmLogin();
            tela.Show();
            Close();
        }
    }
}
