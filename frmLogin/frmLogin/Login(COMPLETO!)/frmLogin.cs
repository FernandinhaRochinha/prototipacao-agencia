﻿using frmLogin.Login;
using frmLogin.Login_COMPLETO__;
using frmLogin.Telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginDTO login = new LoginDTO();
            login.Usuario = txtlogin.Text;
            login.Senha = txsenha.Text;

            LoginBusiness bs = new LoginBusiness();
            bool registro = bs.VerificarRegistro(login);
            if (registro == true)
            {
                frmInicial tela = new frmInicial();
                tela.Show();
                Hide();
            }
            else
            {
                MessageBox.Show("esse usuario nao existe!!");
            }

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CadastrarUsuario tela = new CadastrarUsuario();
            tela.Show();
            Hide();
        }
    }
}
