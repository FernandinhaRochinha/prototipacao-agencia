﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Login
{
    class LoginDatabase
    {
        public bool VerificarRegistro(LoginDTO dto)
        {
            string script = @"SELECT * FROM tb_login WHERE nm_usuario = @nm_usuario AND 
                            nm_senha = @nm_senha";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("nm_senha", dto.Senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if(reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }




        }

        public int Salvar(LoginDTO dto)
        {
            string script = @"insert into tb_login
                            (
                            nm_usuario,
                            nm_senha                          
                     
                            )
                            values 
                            (
                            @nm_usuario,
                            @nm_senha
                            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("nm_senha", dto.Senha));
            


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }
}
