﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Login
{
    class LoginBusiness
    {
        LoginDatabase db = new LoginDatabase();

        public bool VerificarRegistro(LoginDTO dto)
        {
            
            return db.VerificarRegistro(dto);
        }

        public int Salvar(LoginDTO dto)
        {
            return db.Salvar(dto);
        }
    }
}
