﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Vendas_COMPLETO__.Serviços.Cadastro_de_Pacotes
{
    public partial class CadastrarPacotes : Form
    {
        public CadastrarPacotes()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
            PacotesCadastroDTO dto=  new PacotesCadastroDTO();
            dto.Nome = txtNome.Text;
            dto.PrecoNoite = Convert.ToDecimal(txtPrecoNoite.Text);
            dto.PrecoPessoa = Convert.ToDecimal(txtPrecoPessoa.Text);
            dto.Localizacao = txtLocalizacao.Text;
            dto.Hotel = txtHotel.Text;

            PacotesCadastrarBusiness bs = new PacotesCadastrarBusiness();
            bs.Salvar(dto);

            MessageBox.Show("Salvo com sucesso", "Sonhe Alto Viagens", MessageBoxButtons.OK, MessageBoxIcon.Information);


        }
    }
    }
}
