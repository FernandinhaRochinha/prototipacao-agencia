﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Vendas_COMPLETO__.Serviços.Cadastro_de_Pacotes
{
    class PacotesCadastrarBusiness
    {
        PacotesCadastrarDatabase db = new PacotesCadastrarDatabase();

        public int Salvar(PacotesCadastroDTO dto)
        {
            return db.Salvar(dto);
        }
        public void Remover(int ID)
        {
            db.Remover(ID);

        }

        public List<PacotesCadastroDTO> Listar()
        {
            return db.Listar();
        }
        public void Alterar(PacotesCadastroDTO dto)
        {
            db.Alterar(dto);
        }
        public List<PacotesCadastroDTO> Consultar(PacotesCadastroDTO dto)
        {
            return db.Consultar(dto);
        }
    }
}
