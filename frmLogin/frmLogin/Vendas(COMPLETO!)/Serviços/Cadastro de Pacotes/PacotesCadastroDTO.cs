﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Vendas_COMPLETO__.Serviços.Cadastro_de_Pacotes
{
    public class PacotesCadastroDTO
    {
        public int ID { get; set; }

        public string Nome  { get; set; }

        public decimal PrecoPessoa { get; set; }

        public decimal PrecoNoite { get; set; }

        public string Localizacao { get; set; }

        public string Hotel { get; set; }

    }
}
