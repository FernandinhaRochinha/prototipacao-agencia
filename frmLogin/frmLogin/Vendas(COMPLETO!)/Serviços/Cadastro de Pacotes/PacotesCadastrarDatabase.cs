﻿using frmLogin.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Vendas_COMPLETO__.Serviços.Cadastro_de_Pacotes
{
    class PacotesCadastrarDatabase
    {
        Database db = new Database();

        public int Salvar(PacotesCadastroDTO dto)
        {
            string script = @"INSERT INTO tb_pacotes(nm_pacotes, vl_precopessoa, vl_preconoite, ds_localizacao, nm_hotel) VALUES 
                                                    (@nm_pacotes, @vl_precopessoa,@vl_preconoite, @ds_localizacao, @nm_hotel)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_pacotes", dto.Nome));
            parms.Add(new MySqlParameter("vl_precopessoa", dto.PrecoPessoa));
            parms.Add(new MySqlParameter("vl_preconoite", dto.PrecoNoite));
            parms.Add(new MySqlParameter("ds_localizacao", dto.Localizacao));
            parms.Add(new MySqlParameter("nm_hotel", dto.Hotel));

            return db.ExecuteInsertScriptWithPk(script, parms);


        }
        public List<PacotesCadastroDTO> Listar()
        {
            string script = "select * from tb_pacotes";
            List<MySqlParameter> parms = new List<MySqlParameter>();


           
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PacotesCadastroDTO> lista = new List<PacotesCadastroDTO>();

            while (reader.Read())
            {
                PacotesCadastroDTO dto = new PacotesCadastroDTO();
                dto.ID = reader.GetInt32("id_pacotes");
                dto.Nome = reader.GetString("nm_pacotes");
                dto.PrecoPessoa = reader.GetDecimal("vl_precopessoa");
                dto.PrecoNoite = reader.GetInt32("vl_preconoite");
                dto.Localizacao = reader.GetString("ds_localizacao");
                dto.Hotel= reader.GetString("nm_hotel");


                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_pacotes WHERE id_pacotes = @id_pacotes";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pacotes", ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public void Alterar(PacotesCadastroDTO dto)
        {
            string script = @"UPDATE tb_pacotes
                                SET id_pacotes = @id_pacotes,
                                    nm_pacotes = @nm_pacotes , 
                                    vl_precopessoa = @vl_precopessoa ,
                                    vl_preconoite = @vl_preconoite,
                                    ds_localizacao = @ds_localizacao,
                                    nm_hotel = @nm_hotel
                                WHERE id_pacotes = @id_pacotes";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pacotes", dto.ID));
            parms.Add(new MySqlParameter("nm_pacotes", dto.Nome));
            parms.Add(new MySqlParameter("vl_precopessoa", dto.PrecoPessoa));
            parms.Add(new MySqlParameter("vl_preconoite", dto.PrecoNoite));
            parms.Add(new MySqlParameter("ds_localizacao", dto.Localizacao));
            parms.Add(new MySqlParameter("nm_hotel", dto.Hotel));




            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public List<PacotesCadastroDTO> Consultar(PacotesCadastroDTO dto)
        {
            string script = "select * from tb_pacotes WHERE ds_localizacao = @ds_localizacao";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_localizacao", "%" + dto.Localizacao + "%"));
            
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PacotesCadastroDTO> lista = new List<PacotesCadastroDTO>();

            while (reader.Read())

            {
                PacotesCadastroDTO dtos = new PacotesCadastroDTO();
                dto.ID = reader.GetInt32("id_pacotes");
                dto.Nome = reader.GetString("nm_pacotes");
                dto.PrecoPessoa = reader.GetDecimal("vl_precopessoa");
                dto.PrecoNoite = reader.GetInt32("vl_preconoite");
                dto.Localizacao = reader.GetString("ds_localizacao");
                dto.Hotel = reader.GetString("nm_hotel");
            }
            reader.Close();
            return lista;
        }
    }
}
