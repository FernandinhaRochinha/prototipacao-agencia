﻿using frmLogin.Vendas_COMPLETO__.Serviços.Cadastro_de_Pacotes;
using frmLogin.Vendas_COMPLETO__.Serviços.Pacotes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Vendas_COMPLETO__.Serviços
{
    public partial class Pacotes1 : Form
    {
        public Pacotes1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            this.Consultar();
        }

     

        private void Pacotes1_Load(object sender, EventArgs e)
        {

        }
        void Consultar()
        {
            PacotesCadastrarBusiness business = new PacotesCadastrarBusiness();
            List<PacotesCadastroDTO> listar = business.Listar();

            dgvPacotes.AutoGenerateColumns = false;
            dgvPacotes.DataSource = listar;
        }
    }
    
}
